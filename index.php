<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Al Quds AIS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/index.css">
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <style>

        </style>
	</head>
	<body>

<div class="navbar navbar-fixed-top alt" data-spy="affix" data-offset-top="1000">
  <div class="container">
    <div class="navbar-header">
      <a href="#" class="navbar-brand">Al Quds</a>
      <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
      <ul class="nav navbar-nav">
        <li><a href="#sec1">Home</a></li>
        <li><a href="#sec2">About Us</a></li>
        <li><a href="#sec4">Contact Us</a></li>
				<li><a href="apply.php">Apply</a></li>
			</ul>
          <div class="nav navbar-nav navbar-right" id="btnLogin">
            <a class="btn btn-primary navbar-btn" href="login.php">Login</a>
          </div>
        </div>
   </div>
</div>



<div id="sec1" class="header alt vert">
  <div class="container">

    <h1>Al Quds</h1>
      <p class="lead">Arabic International School</p>
      <div>&nbsp;</div>
      <a href="apply.php" class="btn btn-default btn-lg">Apply Now</a>
  </div>
</div>

<div id="sec2" class="padded">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>About Us</h1>
          <hr style="">
        <p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>

            <p class="lead">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
      </div>
<!--
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
-->
    </div>
  </div>
</div>
<!--End of About Us-->
<div class="callout" id="sec3">
  <div class="vert">
    <div class="col-md-12 text-center"><h2>A school like no other</h2></div>
    <div class="col-md-12 text-center">&nbsp;</div>

  </div>
</div>

<div class="map" id="sec4">
    <!-- <h6 class="">Map HERE</h6> -->
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.703554254879!2d79.87086200000002!3d6.925994999999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae259096e4db84b%3A0x62494679349066b5!2sAl+Quds+Arabic+International+School!5e0!3m2!1sen!2slk!4v1434331029743" width="100%" height="100%" frameborder="0" style="border:0"></iframe>
    <br>

    <div class="container">
        <div class="row">
            <div classs="col-lg-12">
                <small>
                    <a href="https://www.google.lk/maps/place/Al+Quds+Arabic+International+School/@6.925995,79.870862,17z/data=!3m1!4b1!4m2!3m1!1s0x3ae259096e4db84b:0x62494679349066b5?hl=en">View Full Map</a>
                </small>
            </div>
        </div>
    </div>
</div>



<div class="container padded">
    <div class="row">
        <div class="col-sm-8">
            <h2>Contact Us</h2>
            <hr>
            <p>Your enquiries here:</p>
            <form class="form-horizontal tpad" role="form">
                <!--  Name-->
                <div class="form-group">
                    <label for="contactName" class="col-lg-2 control-label">Name</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="contactName" placeholder="Name">
                    </div>
                </div>
                <!--  Email  -->
                <div class="form-group">
                    <label for="contactEmail" class="col-lg-2 control-label">Email</label>
                    <div class="col-lg-10">
                        <input type="email" class="form-control" id="contactEmail" placeholder="Email">
                    </div>
                </div>
                <!--  Message  -->
                <div class="form-group tpad">
                    <label for="contactMessage" class="col-lg-2 control-label">Message</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" rows="6" id="contactMessage" placeholder="Message..."></textarea>
                    </div>
                </div>
                <!--  Button  -->
                <div class="form-group tpad">
                    <div class="col-lg-offset-2 col-lg-10">
                        <a data-toggle="modal" href="#modalThanks" class="btn btn-default">Send</a>
                    </div>
                </div>
            </form>

        </div>

        <div class="col-sm-4">
            <!--  Sidebar here -->
            <img class="img-circle img-responsive" src="dist/images/alquds%20school.png">
            <hr>
            <address>
                <h3>Al Quds AIS</h3>
                200 A B C<br>
                Maradana, Colombo 10<br>
                <a href="mailto: swedexit@gmail.com">swedexit@gmail.com</a><rb>
                <abbr title="Phone">P:</abbr>077 9044988
            </address>
        </div>
    </div>
</div>










    <!-- Modal Thanks -->
    <div id="modalThanks" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="moodalThanksLabel" aria-hidden="true">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Thank you for submitting</h3>
            </div>
            <div class="modal-body">
                <p>We appreciate you getting in touch. Please be patient, we will contact you shortly with a reply.</p>
                <p>Al Quds AIS</p>
            </div>
            <div class="modal-footer">
                <a href="index.html" class="btn btn-default btn-lg">OK</a>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
        </div>
    </div>
    <!-- Modal End -->
    <!--  Contact End  -->

<!--
<div class="blurb" id="sec4">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1>Contact Us</h1>
        <img src="" class="img-responsive">
      </div>
    </div>
  </div>
</div>
-->

<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
<!--
        <ul class="list-inline">
          <li><i class="icon-facebook icon-2x"></i></li>
          <li><i class="icon-twitter icon-2x"></i></li>
          <li><i class="icon-google-plus icon-2x"></i></li>
          <li><i class="icon-pinterest icon-2x"></i></li>
        </ul>
        <hr>
-->
        <p>Built by <i class=""></i> <a href="http://www.swedexit.com">Swedex IT</a>.<br> ©2015 </p>
      </div>
    </div>
  </div>
</footer>


</body>
<!-- Script tags Here -->
<script src="dist/js/vendors/jquery/jquery-1.11.2.min.js"></script>
<script src="dist/js/vendors/bootstrap/bootstrap.min.js"></script>
<script src="dist/js/index.js"></script>
</html>
