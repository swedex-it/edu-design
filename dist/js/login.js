$(function(){

  $("#btnLogin").on("click", function(){
    Pace.restart();
    Pace.on("done",function(){
      console.log("login done");
      $("#loginForm").submit();
    })
  });

  $("#btnForgotPassword").on("click", function(){
    Pace.restart();
    Pace.on("done",function(){
      console.log("Forgot Password reset link sent");
    });
    
    Pace.on("hide", function(){
      alert("Reset link sent to your Email ID");
    })
  });
});
