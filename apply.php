<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Al Quds AIS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="dist/css/bootstrap.min.css">
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="dist/css/EDU.css">
    <link rel="stylesheet" href="dist/css/index.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>

<div class="navbar navbar-fixed-top alt" data-spy="affix" data-offset-top="1000">
  <div class="container">
    <div class="navbar-header">
      <a href="index.php" class="navbar-brand">Al Quds</a>
      <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
      <ul class="nav navbar-nav">
        <li><a href="index.php">Home</a></li>
        <li><a href="index.php#sec2">About Us</a></li>
        <li><a href="apply.php">Apply</a></li>
        <li><a href="index.php#sec4">Contact Us</a></li>
      </ul>
          <div class="nav navbar-nav navbar-right" id="btnLogin">
            <a class="btn btn-primary navbar-btn" href="login.php">Login</a>
          </div>
        </div>
   </div>
</div>



        <!-- Main content -->
    <section class="content">
<!--
        <div class="page-header">
            <h1>Apply Now</h1>
            <p class="lead">Apply your deails here</p>
        </div>
			-->
        <div class="col-md-10 col-md-offset-1">
          <form>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Personal Details</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="firstname">First Name</label>
                      <input type="text" class="form-control" id="firstname" placeholder="First Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="middlename">Middle Name</label>
                      <input type="text" class="form-control" id="middlename" placeholder="Middle Name">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastname">Last Name</label>
                      <input type="text" class="form-control" id="lastname" placeholder="Last Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="dateofbirth">Date of Birth</label>
                      <input type="date" class="form-control date" id="dateofbirth" >
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="birthplace">Birth Place</label>
                      <input type="text" class="form-control" id="birthplace" placeholder="Birth Place">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="gender">Gender</label>
                      <select class="form-control" id="gender">
                        <option>Male</option>
                        <option>Female</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nationality">Nationality</label>
                      <input type="text" class="form-control" id="nationality" placeholder="Nationality">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="language">Language</label>
                      <input type="text" class="form-control" id="language" placeholder="Language">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="religion">Religion</label>
                      <select class="form-control" id="religion">
                        <option>Islam</option>
                        <option>Christianity</option>
                        <option>Buddhism</option>
                        <option>Hinduism</option>
                        <option>Other</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="category">Student Category</label>
                      <select class="form-control" id="category">
                        <option>Full Payment</option>
                        <option>Half Payment</option>
                        <option>Orphan</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Contact Details</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="address1">Address Line 1</label>
                      <input type="text" class="form-control" id="address1" placeholder="Address Line 1">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="address2">Address Line 2</label>
                      <input type="text" class="form-control" id="address2" placeholder="Address Line 2">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="city">City</label>
                      <input type="text" class="form-control" id="city" placeholder="City">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="zip">Zip Code</label>
                      <input type="text" class="form-control" id="zip" placeholder="Zip Code">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="country">Country</label>
                      <input type="text" class="form-control" id="country" placeholder="Country">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="phone1">Phone 1</label>
                      <input type="text" class="form-control" id="phone1" placeholder="Phone 1">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="phone2">Phone 2</label>
                      <input type="text" class="form-control" id="phone2" placeholder="Phone 2">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="email">Email Address</label>
                      <input type="text" class="form-control" id="email" placeholder="Email Address">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                  <h3 class="panel-title">Additional Information</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="institute">Previous School</label>
                      <input type="text" class="form-control" id="institute" placeholder="Previous School">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="grade">Grade</label>
                      <input type="text" class="form-control" id="grade" placeholder="Grade">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row bpad">
              <div class="col-xs-offset-5 col-xs-2">
                <a href="#" class="btn btn-primary btn-lg">Submit <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
          </form>
        </div>
    </div>
  </section><!-- /.content -->



<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
<!--
        <ul class="list-inline">
          <li><i class="icon-facebook icon-2x"></i></li>
          <li><i class="icon-twitter icon-2x"></i></li>
          <li><i class="icon-google-plus icon-2x"></i></li>
          <li><i class="icon-pinterest icon-2x"></i></li>
        </ul>
        <hr>
-->
        <p>Built by <i class=""></i> <a href="http://www.swedexit.com">Swedex IT</a>.<br> ©2015 </p>
      </div>
    </div>
  </div>
</footer>


</body>
<!-- Script tags Here -->
<script src="dist/js/vendors/jquery/jquery-1.11.2.min.js"></script>
<script src="dist/js/vendors/bootstrap/bootstrap.min.js"></script>
<script src="dist/js/index.js"></script>
</html>
