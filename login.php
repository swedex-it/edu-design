<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Al Quds Login</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="dist/css/bootstrap.min.css">
  	<link rel="stylesheet" href="dist/css/login_forgotpass.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="loginTitle">
      <h1>Al Quds Login Panel</h1>
    </div>
    <div class="loginArrow"></div>
    <div class="loader"></div>
    <div class="counter bpad"></div>
    <div class="container">
      <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
          <div class="loginBox">
            <form class="form" action="dashboard/index.php" method="post" id="loginForm">
              <div class="input-group input-group-sm">
                <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-user"></span></span>
                <input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon1" required>
              </div>
              <div class="input-group input-group-sm">
                <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-lock"></span></span>
                <input type="text" class="form-control" placeholder="Password" aria-describedby="sizing-addon1" required>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>
              <input type="button" class="form-control btn btn-success" value="Login" id="btnLogin">
              <a type="button" class="form-control btn btn-link" href="forgotpassword.php">Forgot Password ?</a>
          </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Script tags Here -->
    <script src="dist/js/vendors/jquery/jquery-1.11.2.min.js"></script>
    <script src="dist/js/vendors/bootstrap/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <script src="dist/js/login.js"></script>
  </body>
</html>
