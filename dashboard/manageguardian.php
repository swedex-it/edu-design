<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Guardian
        <small>Manage parent details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">Parent Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-body"> <!-- Start Panel Body -->
              <div class="row">
               <p class="col-xs-12 col-md-4">
                 <input type="text" id="filter" class="form-control" placeholder="Search.."/>
               </p>
             </div>
             <div class="table-responsive">
               <table class="table foo table-bordered" data-filter=#filter data-page-navigation=".pagination">
                 <thead>
                  <tr>
                    <th data-toggle="true">First Name</th>
                    <th>Last Name</th>
                    <th data-hide="phone,tablet">Relation</th>
                    <th data-hide="phone,tablet">Email</th>
                    <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Tiger</td>
                    <td>Nixon</td>
                    <td>Father</td>
                    <td>test@test.com</td>
                    <td>
                      <a class="btn btn-warning btn-sm" href="guardian.php"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                      <button class="btn btn-danger btn-sm" onclick="alert('prompt confirmation message')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Garrett Winters</td>
                    <td>Accountant</td>
                    <td>Tokyo</td>
                    <td>63</td>
                    <td>
                      <a class="btn btn-warning btn-sm" href="guardian.php"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                      <button class="btn btn-danger btn-sm" onclick="alert('prompt confirmation message')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Ashton Cox</td>
                    <td>Junior Technical Author</td>
                    <td>San Francisco</td>
                    <td>66</td>
                    <td>
                      <a class="btn btn-warning btn-sm" href="guardian.php"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                      <button class="btn btn-danger btn-sm" onclick="alert('prompt confirmation message')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Cedric Kelly</td>
                    <td>Senior Javascript Developer</td>
                    <td>Edinburgh</td>
                    <td>22</td>
                    <td>
                      <a class="btn btn-warning btn-sm" href="guardian.php"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                      <button class="btn btn-danger btn-sm" onclick="alert('prompt confirmation message')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Airi Satou</td>
                    <td>Accountant</td>
                    <td>Tokyo</td>
                    <td>33</td>
                    <td>
                      <a class="btn btn-warning btn-sm" href="guardian.php"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                      <button class="btn btn-danger btn-sm" onclick="alert('prompt confirmation message')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Brielle Williamson</td>
                    <td>Integration Specialist</td>
                    <td>New York</td>
                    <td>61</td>
                    <td>
                      <a class="btn btn-warning btn-sm" href="guardian.php"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                      <button class="btn btn-danger btn-sm" onclick="alert('prompt confirmation message')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="row">
             <div class="col-xs-12 text-center">
              <ul class="pagination"></ul>
            </div>
          </div>
        </div> <!-- End Panel Body -->
      </div>
    </div>
  </div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php include "includes/_footer.php"; ?>

<?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
