<?php include "includes/_header.php"; ?>
<div class="wrapper">

	<?php include "includes/_nav.php"; ?>

	<aside class="main-sidebar">
		<?php include "includes/_sidebar.php"; ?>
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">

		<!-- create new leave type modal form -->
		<div class="modal fade" id="modalAddLeaveType">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Create New Leave Type</h4>
					</div>
					<form>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="leaveName">Leave Name</label>
										<input type="text" class="form-control" id="leaveName" placeholder="Leave Name">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="leaveCode">Leave Code</label><br>
										<input type="text" class="form-control" id="leaveCode" placeholder="Leave Code">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="maxLeave">Max Leave Count</label>
										<input type="text" class="form-control" id="maxLeave" placeholder="Max Leave Count">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="leaveStatus">Leave Status</label><br>
										<select class="form-control" id="leaveStatus">
											<option>Active</option>
											<option>Inactive</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Staff
				<small>Manage Leave Types</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
				<li class="active">Staff</li>
				<li class="active">Leave Types</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<div class="row">
				<div class="col-md-12 ">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="text-right">
								<button class="btn btn-primary" data-toggle="modal" data-target="#modalAddLeaveType">New Leave Type</button>
							</div>
						</div>
						<div class="panel-body">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table foo table-bordered">
										<thead>
											<tr>
												<th data-toggle="true">Leave Type</th>
												<th>Status</th>
												<th data-hide="phone,tablet" data-sort-ignore="true">Actions</th>
											</tr>
										</thead>
										<tr>
											<td>Sick</td>
											<td>Active</td>
											<td>
												<button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
												<button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
											</td>
										</tr>
										<tr>
											<td>Casual</td>
											<td>Active</td>
											<td>
												<button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
												<button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
											</td>
										</tr>
										<tr>
											<td>Maternity</td>
											<td>Inactive</td>
											<td>
												<button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
												<button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
											</td>
										</tr>
										<tr>
											<td>Festival</td>
											<td>Active</td>
											<td>
												<button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
												<button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

	<?php include "includes/_footer.php"; ?>

	<?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
