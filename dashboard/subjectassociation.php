<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Subject Association
        <small>Associate all subjects</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">Subject Association</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">Search Filter</div>
            <div class="panel-body">
              <form class="form-inline">
                <div class="row">
                  <label for="grade" class="col-sm-1 text-right">Grade</label>
                  <div class="col-sm-2">
                    <select class="form-control staff-grade" id="grade">
                      <option>--Select Grade--</option>
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
                  <label for="subject" class="col-sm-1 text-right">Subject</label>
                  <div class="col-sm-2">
                    <select class="form-control staff-subject" id="subject">
                      <option>--Select Subject--</option>
                      <option>English</option>
                      <option>Maths</option>
                      <option>Computer Science</option>
                      <option>Commerce</option>
                      <option>Economics</option>
                    </select>
                  </div>
                </div>
              </form>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          Assigned Staff List:
        </div>
        <div class="panel-body">
          <h4 class="text-center">No Employees Assigned Yet</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Search Filter</h3>
            </div>
            <div class="box-body">
              <form class="form-inline">
                <div class="form-group lpad">
                  <label for="department">Department</label>
                  <select class="form-control" id="department">
                    <option>--Select Department--</option>
                    <option>English</option>
                    <option>Maths</option>
                    <option>Computer Science</option>
                    <option>Commerce</option>
                    <option>Economics</option>
                  </select>
                </div>
              </form>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          Assign New:
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th>Staff Name</th>
                  <th data-sort-ignore="true">Action</th>
                </tr>
              </thead>
              <tr>
                <td>Rajesh Anaja</td>
                <td><a href="#" onclick="confirm('Are you sure?');">Assign</a></td>
              </tr>
              <tr>
                <td>Ramjan Razee</td>
                <td><a href="#" onclick="confirm('Are you sure?');">Assign</a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
