<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Staff
        <small>Add new staff</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Staff</li>
        <li class="active">New Staff</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-offset-4 col-md-8">
          <div class="col-md-3">
            <button type="button" class="btn btn-primary btn-nav">Staff Details</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Staff Contact Details</button>
          </div>
        </div>
      </div>
      <br />
      <div class="row">
        <div class="col-md-12">
          <form>
            <div  class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">General Details</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="staffId">Staff ID</label>
                      <input type="text" class="form-control" id="staffId" placeholder="Staff ID">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="joiningDate">Joining Date</label><br>
                      <input type="date" class="form-control date" id="joiningDate">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="firstname">First Name</label>
                      <input type="text" class="form-control" id="firstname" placeholder="First Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="middlename">Middle Name</label>
                      <input type="text" class="form-control" id="middlename" placeholder="Middle Name">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastname">Last Name</label>
                      <input type="text" class="form-control" id="lastname" placeholder="Last Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="staffDepartment">Staff Department</label>
                      <select class="form-control" id="staffDepartment">
                        <option>--Select Department--</option>
                        <option>Mathematics</option>
                        <option>Science</option>
                        <option>Computer Science</option>
                        <option>English</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="dateofbirth">Date of Birth</label>
                      <input type="date" class="form-control date" id="dateofbirth" >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="gender">Gender</label>
                      <select class="form-control" id="gender">
                        <option>Male</option>
                        <option>Female</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="employeePosition">Employee Position</label>
                      <select class="form-control" id="employeePosition">
                        <option>--Select Staff Position--</option>
                        <option>HOD</option>
                        <option>Junior Lecturer</option>
                        <option>Seniro Lecturer</option>
                        <option>Administrator</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="employeeGrade">Employee Grade</label>
                      <select class="form-control" id="employeeGrade">
                        <option>--Select Grade--</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="jobTitle">Job Title</label>
                      <input type="text" class="form-control" id="jobTitle" placeholder="Job Title">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="qualification">Qualification</label>
                      <input type="text" class="form-control" id="qualification" placeholder="Qualification">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="totalYears">Total Experience</label>
                      <select class="form-control" id="totalYears">
                        <option>--Year--</option>
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="totalMonths"></label>
                      <select class="form-control" id="totalMonths">
                        <option>--Month--</option>
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="experienceDetails">Experience Details</label>
                      <textarea class="form-control" rows="4" id="experienceDetails"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">Personal Details</div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="martialStatus">Marital Status</label>
                    <select class="form-control" id="martialStatus">
                      <option>--Marital Status--</option>
                      <option>Single</option>
                      <option>Married</option>
                      <option>Divorced</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="childrenCount">Children Count</label>
                    <input type="text" class="form-control" id="childrenCount" placeholder="Children Count">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="fatherName">Father's Name</label>
                    <input type="text" class="form-control" id="fatherName" placeholder="Father's Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="motherName">Mother's Name</label>
                    <input type="text" class="form-control" id="motherName" placeholder="Mother's Name">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="bloodgroup">Blood Group</label>
                    <select class="form-control" id="bloodgroup">
                      <option>--Blood Group--</option>
                      <option>AB+</option>
                      <option>AB-</option>
                      <option>A+</option>
                      <option>A-</option>
                      <option>B+</option>
                      <option>B-</option>
                      <option>O+</option>
                      <option>O-</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="spouseName">Spouse Name</label>
                    <input type="text" class="form-control" id="spouseName" placeholder="Spouse Name">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="nationality">Nationality</label>
                    <select class="form-control" id="nationality">
                      <option>--Select Nationality--</option>
                      <option>Sri Lanka</option>
                      <option>India</option>
                      <option>Pakistan</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="staffPhotoUpload">Upload Photo</label>
                    <form action="" method="post" enctype="multipart/form-data" id="js-upload-form">
                      <div class="form-inline">
                        <div class="form-group">
                          <input type="file" name="files[]" id="js-upload-files" multiple>
                        </div>
                        <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit">Upload files</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-md-offset-5">
              <!-- <input type="submit" class="btn-primary" value="DOCUMENTS  >" > -->
              <a href="staffcontact.php" class="btn btn-primary btn-lg">Contact Details <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
