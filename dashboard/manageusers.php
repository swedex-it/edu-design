<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Users
        <small>Manage all users</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">Manage Users</li>
      </ol>
    </section>

    <!-- Model form here -->

    <div class="modal fade" id="viewUser">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">View User  - Hisham Haniffa</h4>
          </div>
          <div class="modal-body">
            <div class="table-responsive">
              <table class="table foo table-bordered table-condensed">
                <tr>
                  <th>Username</th>
                  <td>Admin</td>
                </tr>
                <tr>
                  <th>First Name</th>
                  <td>Hisham</td>
                </tr>
                <tr>
                  <th>Last Name</th>
                  <td>Haniffa</td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td>hishamhaniffa4@gmail.com</td>
                </tr>
                <tr>
                  <th>Created At</th>
                  <td>Oct 04.2012 06:29 AM</td>
                </tr>
                <tr>
                  <th>last At</th>
                  <td>Sep 11.2015 03:59 PM</td>
                </tr>
                <tr>
                  <th>Superuser</th>
                  <td>Yes</td>
                </tr>
                <tr>
                  <th>Status</th>
                  <td>Active</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- End Modal form here -->

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <a class="btn btn-primary" href="createnewuser.php">Create User</a>
          </div>
        </div>
        <div class="panel-body">
          <div class="row">
            <p class="col-xs-12 col-md-4">
              <input type="text" class="form-control" id="filter" placeholder="Search.."/>
            </p>
          </div>
          <div class="table-responsive">
            <table class="table foo table-bordered" data-filter=#filter data-page-navigation=".pagination">
              <thead>
                <tr>
                  <th data-toggle="true">Name</th>
                  <th data-toggle="true">Role</th>
                  <th data-hide="phone">Email</th>
                  <th data-hide="phone">Last Visit</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Hisham Haniffa</td>
                  <td>Admin</td>
                  <td>hishamhaniffa4@gmail.com</td>
                  <td>Sep 11.2015 03:59 PM</td>
                  <td>
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#viewUser"><span class="glyphicon glyphicon-list-alt"></span> View</button>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#viewUser"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                    <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>Muhammad Muhsin</td>
                  <td>Admin</td>
                  <td>hishamhaniffa4@gmail.com</td>
                  <td>Sep 11.2015 03:59 PM</td>
                  <td>
                    <button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-list-alt"></span> View</button>
                    <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                    <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>Abdul Raheem</td>
                  <td>Teacher</td>
                  <td>hishamhaniffa4@gmail.com</td>
                  <td>Sep 11.2015 03:59 PM</td>
                  <td>
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#viewUser"><span class="glyphicon glyphicon-list-alt"></span> View</button>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#viewUser"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                    <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>Humaiz Azad</td>
                  <td>Student</td>
                  <td>hishamhaniffa4@gmail.com</td>
                  <td>Sep 11.2015 03:59 PM</td>
                  <td>
                    <button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-list-alt"></span> View</button>
                    <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                    <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="row">
            <div class="col-xs-12 text-center">
              <ul class="pagination"></ul>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
