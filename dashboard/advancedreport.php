<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Advanced Report
        <small>Student Advanced Report</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Report</li>
        <li class="active">Advanced Report</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Search Filter
        </div>
        <div class="panel-body">
          <div class="row">
            <form>
              <div class="col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="searchByName" class="control-label">Name</label>
                  <input type="text" class="form-control" id="searchByName" placeholder="Name" />
                </div>
                <div class="form-group">
                  <label for="searchByAdmissionNo" class="control-label">Admission No</label>
                  <input type="text" class="form-control" id="searchByAdmissionNo" placeholder="Admission No" />
                </div>
                <div class="form-group">
                  <label for="searchByEmail" class="control-label">Email</label>
                  <input type="email" class="form-control" id="searchByEmail" placeholder="Email" />
                </div>
              </div>
              <div class="col-sm col-md-6">
                <div class="form-group">
                  <label for="searchByGender" class="control-label">Gender</label>
                  <select id="searchByGender" class="form-control">
                    <option>All</option>
                    <option>Male</option>
                    <option>Female</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="searchByBloodGroup" class="control-label">Blood Group</label>
                  <select id="searchByBloodGroup" class="form-control">
                    <option>A+</option>
                    <option>A-</option>
                    <option>B+</option>
                    <option>B-</option>
                    <option>O+</option>
                    <option>O-</option>
                    <option>AB+</option>
                    <option>AB-</option>
                  </select>
                </div>
                <div class="form-group tpad-25">
                  <label>
                    <input type="checkbox" />&nbsp; Include guardian details
                  </label>
                </div>
              </div>
              <div class="col-sm-12">
                <a href="#" class="btn btn-primary btn-lg" onclick="$('#resultset').css('display','block');$('.table.foo').trigger('footable_initialize');">Search</a>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="panel panel-primary hide-me animated fadeInUp" id="resultset">
        <div class="panel-heading">Search Results</div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered table-striped text-center">
              <thead>
                <tr>
                  <th data-toggle="true">Admission No</th>
                  <th>Student Name</th>
                  <th data-hide="phone">Grade/Batch</th>
                  <th data-hide="phone">Gender</th>
                </tr>
              </thead>
              <tr>
                <td><a href="studentpreview.php">8</a></td>
                <td><a href="studentpreview.php">Humaiz Azad</a></td>
                <td>Grade 10/A</td>
                <td>Male</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
<script type="text/javascript">

$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square',
    increaseArea: '10%' // optional
  });
});

</script>
