<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- student category modal form -->
    <div class="modal fade" id="modalStudentCategory">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create New Student Category</h4>
          </div>
          <form>
            <div class="modal-body">
              <div class="form-group">
                <label for="txtStudentCatName">Name</label>
                <input type="text" class="form-control" id="txtStudentCatName" placeholder="Student Category">
              </div>
              <div class="form-group">
                <label for="txtFeeExemption">Fee Exemption</label>
                <input type="text" class="form-control" id="txtFeeExemption" placeholder="Fee Exemption">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student
        <small>Manage student category</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">Student category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <div class="text-right">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalStudentCategory">Create New Category</button>
              </div>
            </div>
            <div class="panel-body"> <!-- Panel Body -->
              <div class="table-responsive">
                <table class="table foo table-bordered" data-filter=#filter data-page-navigation=".pagination">
                  <thead>
                    <tr>
                    <th data-toggle="true">Category Name</th>
                      <th>No. of Students</th>
                      <th data-hide="phone" data-sort-ignore="true">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Full Payment</td>
                      <td><span class="badge">200</span></td>
                      <td>
                        <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                    <tr>
                      <td>Half Payment</td>
                      <td><span class="badge">45</span></td>
                      <td>
                        <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                    <tr>
                      <td>Orphan</td>
                      <td><span class="badge">10</span></td>
                      <td>
                        <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div> <!-- /Panel Body -->
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
