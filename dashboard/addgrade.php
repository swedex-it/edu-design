<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Grade
        <small>Add New Grade Details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Grade</li>
        <li class="active">Create Grade</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">New Grade</div>
            <div class="panel-body">
              <form class="form-horizontal">
                <div class="form-group required">
                  <label for="gradeName" class="col-sm-2 control-label">Grade</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="gradeName" placeholder="Grade">
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading">New Batch</div>
            <div class="panel-body">
              <form class="form-horizontal">
                <div class="form-group required">
                  <label for="batchName" class="col-sm-2 control-label">Batch</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="batchName" placeholder="Batch">
                  </div>
                </div>
                <div class="form-group required">
                  <label for="batchYear" class="col-sm-2 control-label">Batch Year</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="batchYear">
                      <option>2015</option>
                      <option>2016</option>
                      <option>2017</option>
                    </select>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-2 col-md-offset-5">
              <!-- <input type="submit" class="btn-primary" value="DOCUMENTS  >" > -->
              <a href="staffcontact.php" class="btn btn-primary btn-lg">Save <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
