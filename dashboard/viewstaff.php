<?php include "includes/_header.php"; ?>
<div class="wrapper">

	<?php include "includes/_nav.php"; ?>

	<aside class="main-sidebar">
		<?php include "includes/_sidebar.php"; ?>
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Staff
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
				<li class="active">Staff</li>
				<li class="active">Staff Leave Types</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							View Staff
						</div>
						<div class="panel-body">
							<div class="row">
								<p class="col-xs-12 col-md-4">
									<input type="text" id="filter" class="form-control" placeholder="Search.."/>
								</p>
							</div>
							<div class="table-responsive">
								<table class="table foo table-bordered" data-filter=#filter data-page-navigation=".pagination">
									<thead>
										<tr>
											<th data-toggle="">Staff ID</th>
											<th>Staff Name</th>
											<th data-hide="phone,tablet">Subjects</th>
											<th data-hide="phone,tablet">Gender</th>
											<th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>QD1</td>
											<td>Mr. Gregory</td>
											<td>Mathematics</td>
											<td>Male</td>
											<td>
												<button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
												<button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
											</td>
										</tr>
										<tr>
											<td>QD2</td>
											<td>Mr. Seresinghe</td>
											<td>Accounting</td>
											<td>Male</td>
											<td>
												<button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
												<button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
											</td>
										</tr>
										<tr>
											<td>QD3</td>
											<td>Mr. Hasanar</td>
											<td>Arabic</td>
											<td>Male</td>
											<td>
												<button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
												<button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
											</td>
										</tr>
										<tr>
											<td>QD4</td>
											<td>Mrs. Cooray</td>
											<td>English</td>
											<td>Female</td>
											<td>
												<button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
												<button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-xs-12 text-center">
									<ul class="pagination"></ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

	<?php include "includes/_footer.php"; ?>

	<?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
