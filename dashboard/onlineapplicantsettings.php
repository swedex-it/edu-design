<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Online Registration Settings
        <small>Online applicants Settings</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">Online Registration Settings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <form>
        <div class="panel panel-primary">
          <div class="panel-body">
            <div class="col-xs-12 col-md-4">
              <div class="form-group required">
                <label for="onlineAcademicYear" class="control-label">Academic Year</label>
                <select id="onlineAcademicYear" class="form-control">
                  <option>AY2015</option>
                  <option selected>AY2016</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-md-4">
              <div class="form-group required text-center">
                <label for="AllowOnlineAdmission" class="control-label">Allow Online Admission</label>
                <div class="checkbox">
                  <input type="checkbox" id="AllowOnlineAdmission" />
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-4">
              <div class="form-group required">
                <label for="onlineRegistrationNo" class="control-label">Registration No</label>
                <input type="text" id="onlineRegistrationNo" class="form-control" />
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" /> Show link for Online Admission
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group text-center">
          <button class="btn btn-primary btn-lg">Apply</button>
        </div>
      </form>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
