<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Staff Positions
        <small>All staff Position</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Staff</li>
        <li class="active">Manage Staff Positions</li>
      </ol>
    </section>
    <!-- Modal forms Here-->
    <div class="modal fade" id="editStaffPos">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Position</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="categoryName" >Name</label>
                <input type="text" class="form-control" placeholder="Position Name" value="Senior Lecturer" />
              </div>
              <div class="form-group">
                <label for="categoryPrefix" >Staff Category</label>
                <select class="form-control" id="categoryPrefix">
                  <option>--Select Category--</option>
                  <option>Academic</option>
                  <option>Administrative</option>
                  <option>Teacher</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="newStaffPos">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create New Staff Position</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="categoryName" >Name</label>
                <input type="text" class="form-control" placeholder="Category Name" />
              </div>
              <div class="form-group">
                <label for="categoryPrefix" >Staff Category</label>
                <select class="form-control" id="categoryPrefix">
                  <option>--Select Category--</option>
                  <option>Academic</option>
                  <option>Administrative</option>
                  <option>Teacher</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End of Modal forms -->

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#newStaffPos">Add Position</button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered" id="staffCategory">
              <thead>
                <tr>
                  <th data-toggle="true">Name</th>
                  <th>Staff Category</th>
                  <th data-hide="phone" data-sort-ignore="true">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Senior Lecturer </td>
                  <td>Academic</td>
                  <td>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editStaffPos"><i class="ion-edit"></i> Edit</button>
                    <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><i class="ion-trash-a"></i> Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>Junior Lecturer </td>
                  <td>Academic</td>
                  <td>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editStaffPos"><i class="ion-edit"></i> Edit</button>
                    <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><i class="ion-trash-a"></i> Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>HOD</td>
                  <td>Academic</td>
                  <td>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editStaffPos"><i class="ion-edit"></i> Edit</button>
                    <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><i class="ion-trash-a"></i> Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>Accountant</td>
                  <td>Administrative</td>
                  <td>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editStaffPos"><i class="ion-edit"></i> Edit</button>
                    <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><i class="ion-trash-a"></i> Delete</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
