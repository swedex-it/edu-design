<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Full Timetable
        <small>View All Timetables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Timetable</li>
        <li class="active">View Full Timetablen</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Search Filter
        </div>
        <div class="panel-body">
          <div class="col-sm-8">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="academicYear" class="col-sm-2 control-label">Academic Year</label>
                <div class="col-sm-10">
                  <select id="academicYear" class="form-control">
                    <option>--Select Academic Year--</option>
                    <option>2014</option>
                    <option>2015</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="course" class="col-sm-2 control-label">Course</label>
                <div class="col-sm-10">
                  <select id="course" class="form-control">
                    <option>--Select Grade--</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="batch" class="col-sm-2 control-label">Batch</label>
                <div class="col-sm-10">
                  <select id="batch" class="form-control">
                    <option>--Select Batch--</option>
                    <option>A</option>
                    <option>B</option>
                    <option>C</option>
                    <option>D</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="mode" class="col-sm-2 control-label">Mode</label>
                <div class="col-sm-10">
                  <select id="mode" class="form-control">
                    <option>--Select Mode--</option>
                    <option>Day</option>
                    <option>Week</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="day" class="col-sm-2 control-label">Day</label>
                <div class="col-sm-10">
                  <select id="day" class="form-control">
                    <option>--Select Day--</option>
                    <option>Monday</option>
                    <option>Tuesday</option>
                    <option>Wednesday</option>
                    <option>Thursday</option>
                    <option>Friday</option>
                    <option>Saturday</option>
                    <option>Sunday</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="box box-primary">
        <div class="box-header bg-blue">
          <div class="col-sm-2">
            Grade: <strong>1</strong>
          </div>
          <div class="col-sm-2">
            Batch: <strong>A</strong>
          </div>
          <div class="col-sm-4">
            Class Teacher: <strong>Muhammad Muhsin</strong>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th data-toggle="true" data-sort-ignore="true">Weekdays</th>
                  <th data-hide="phone" data-sort-ignore="true">08:30 AM - 10: 00 AM</th>
                  <th data-hide="phone" data-sort-ignore="true">10:00 AM - 12: 00 PM</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">01:00 PM - 02: 00 PM</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">02:00 PM - 03: 00 PM</th>
                </tr>
              </thead>
              <tr>
                <th>Monday</th>
                <td>Maths</td>
                <td>English</td>
                <td>Science</td>
                <td>Economics</td>
              </tr>
              <tr>
                <th>Tuesday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Wednesday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Thursday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Friday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </table>
          </div><!-- End of timetable -->
        </div>
      </div><!-- /.box -->
      <div class="box box-primary">
        <div class="box-header bg-blue">
          <div class="col-sm-2">
            Grade: <strong>1</strong>
          </div>
          <div class="col-sm-2">
            Batch: <strong>B</strong>
          </div>
          <div class="col-sm-4">
            Class Teacher: <strong>Muhammad Muhsin</strong>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th data-toggle="true" data-sort-ignore="true">Weekdays</th>
                  <th data-hide="phone" data-sort-ignore="true">08:30 AM - 10: 00 AM</th>
                  <th data-hide="phone" data-sort-ignore="true">10:00 AM - 12: 00 PM</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">01:00 PM - 02: 00 PM</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">02:00 PM - 03: 00 PM</th>
                </tr>
              </thead>
              <tr>
                <th>Monday</th>
                <td>Maths</td>
                <td>English</td>
                <td>Science</td>
                <td>Economics</td>
              </tr>
              <tr>
                <th>Tuesday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Wednesday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Thursday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Friday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </table>
          </div><!-- End of timetable -->
        </div>
      </div><!-- /.box -->
      <div class="box box-primary">
        <div class="box-header bg-blue">
          <div class="col-sm-2">
            Grade: <strong>2</strong>
          </div>
          <div class="col-sm-2">
            Batch: <strong>A</strong>
          </div>
          <div class="col-sm-4">
            Class Teacher: <strong>Muhammad Muhsin</strong>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th data-toggle="true" data-sort-ignore="true">Weekdays</th>
                  <th data-hide="phone" data-sort-ignore="true">08:30 AM - 10: 00 AM</th>
                  <th data-hide="phone" data-sort-ignore="true">10:00 AM - 12: 00 PM</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">01:00 PM - 02: 00 PM</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">02:00 PM - 03: 00 PM</th>
                </tr>
              </thead>
              <tr>
                <th>Monday</th>
                <td>Maths</td>
                <td>English</td>
                <td>Science</td>
                <td>Economics</td>
              </tr>
              <tr>
                <th>Tuesday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Wednesday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Thursday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <th>Friday</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </table>
          </div><!-- End of timetable -->
        </div>
      </div><!-- /.box -->

    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
