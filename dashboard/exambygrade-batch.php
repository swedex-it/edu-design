<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Select Grade Batch
        <small>Select Grade and Batch</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Exams</li>
        <li class="active">Select Grade Batch</li>
      </ol>
    </section>

    <!-- Modal form here -->
    <div class="modal fade" id="createNewExam">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create New Exam</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group required">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="name" placeholder="Name">
                </div>
              </div>
              <div class="form-group required">
                <label for="examType" class="col-sm-2 control-label">Exam Type</label>
                <div class="col-sm-10">
                  <select id="examType" class="form-control">
                    <option>Marks</option>
                    <option>Grade</option>
                    <option>Marks And Grade</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> Date Is Published
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> Result Is Published
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group required">
                <label for="examDate" class="col-sm-2 control-label">Exam Date</label>
                <div class="col-sm-10">
                  <input type="date" class="form-control" id="name" placeholder="Exam Date">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- End of Modal form here -->

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#createNewExam">Create Exam</button>
            <a class="btn btn-primary" href="setgradinglevels.php">Grading Levels</a>
          </div>
        </div>
        <div class="panel-body">
          <div class="box box-primary">
            <div class="box-header">
              <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-2">
                  <strong>Grade <label class="label label-primary">Class 10</label></strong>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 hidden-xs">
                  <strong>Batch <label class="label label-primary">A</label></strong>
                </div>
                <!-- only will be visible on xs screen size -->
                <div class="col-xs-12 visible-xs-block tpad">
                  <strong>Batch <label class="label label-primary">A</label></strong>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3 hidden-xs">
                  <strong>Examination <label class="label label-primary">First Term</label></strong>
                </div>
                <!-- only will be visible on xs screen size -->
                <div class="col-xs-12 visible-xs-block tpad">
                  <strong>Examination <label class="label label-primary">First Term</label></strong>
                </div>
              </div>
              <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <button class="btn btn-default" data-toggle="modal" data-target="#modalExamByGradeBatch">Change Batch</button>
              </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive" id="exams">
                <table class="table foo table-bordered">
                  <thead>
                    <tr>
                      <th data-toggle="true">Name</th>
                      <th>Exam Type</th>
                      <th data-hide="phone,tablet">Date Is Published</th>
                      <th data-hide="phone,tablet">Result Is Published</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                      <th data-hide="phone" data-sort-ignore="true">Manage</th>
                    </tr>
                  </thead>
                  <tr>
                    <td>First Term</td>
                    <td>Marks and Grade</td>
                    <td>Yes</td>
                    <td>No</td>
                    <td>
                      <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#createNewExam"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                      <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure you want to delete ?')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                    <td>
                      <button class="btn btn-primary btn-sm" onclick="$('.hide-me').css('display','block');"><span class="glyphicon glyphicon-list-alt"></span> Manage Exam</button>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div><!-- /.box -->
        </div>
      </div>

      <div class="panel panel-primary animated fadeInUp hide-me">
        <div class="panel-heading">
          Enter Exam related details
        </div><!-- /.panel-heading -->
        <div class="panel-body">
          <form class="">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="minMark" class="control-label">Min Mark</label>
                  <input type="text" id="minMark" class="form-control" placeholder="Min Mark"/>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="maxMark" class="control-label">Max Mark</label>
                  <input type="text" id="maxMark" class="form-control" placeholder="Max Mark"/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive">
                  <table class="table foo table-bordered">
                    <thead>
                      <tr>
                        <th data-toggle="true">Subject Name</th>
                        <th data-hide="phone" data-sort-ignore="true">Max Marks</th>
                        <th data-hide="phone" data-sort-ignore="true">Min Marks</th>
                        <th data-hide="phone,tablet" data-sort-ignore="true">Start Time</th>
                        <th data-hide="phone,tablet" data-sort-ignore="true">End Time</th>
                      </tr>
                    </thead>
                    <tr>
                      <td>English</td>
                      <td><input type="text" class="form-control" placeholder="Max Marks"/></td>
                      <td><input type="text" class="form-control" placeholder="Min Marks"/></td>
                      <td><input type="time" class="form-control" placeholder="Start Time"/></td>
                      <td><input type="time" class="form-control" placeholder="End Time"/></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 text-center">
                <button class="btn btn-primary btn-lg">Create</button>
              </div>
            </div>
          </form>
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->

      <div class="panel panel-primary animated fadeInUp hide-me">
        <div class="panel-heading">Scheduled Subjects</div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th data-toggle="true">Subject</th>
                  <th data-hide="phone">Start Time</th>
                  <th data-hide="phone">End Time</th>
                  <th data-hide="phone,tablet">Min Marks</th>
                  <th data-hide="phone,tablet">Max Marks</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">Manage</th>
                </tr>
              </thead>
              <tr>
                <td>Maths</td>
                <td>Sep 9 2015 10:00 AM </td>
                <td>Sep 9 2015 12:00 AM </td>
                <td>0</td>
                <td>100</td>
                <td>
                  <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
                <td>
                  <button class="btn btn-primary btn-sm" onclick="$('.hide-me2').css('display','block')"><span class="glyphicon glyphicon-list-alt"></span> Manage Score</button>
                </td>
              </tr>
              <tr>
                <td>Science</td>
                <td>Sep 9 2015 01:00 PM </td>
                <td>Sep 9 2015 03:00 PM </td>
                <td>0</td>
                <td>100</td>
                <td>
                  <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
                <td>
                  <button class="btn btn-primary btn-sm" onclick="$('.hide-me2').css('display','block')"><span class="glyphicon glyphicon-list-alt"></span> Manage Score</button>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div class="panel panel-primary animated fadeInUp hide-me2">
        <div class="panel-heading">
          Enter Exam Scores
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
              <tr>
                <th>Student Name</th>
                <th data-sort-ignore="true">Marks</th>
                <th data-sort-ignore="true" data-hide="phone">Remarks</th>
              </tr>
              </thead>
              <tr>
                <td>Arun Mohan</td>
                <td><input type="text" class="form-control" placeholder="Marks" /></td>
                <td><input type="text" class="form-control" placeholder="Remarks" /></td>
              </tr>
              <tr>
                <td>Khan Yasir</td>
                <td><input type="text" class="form-control" placeholder="Marks" /></td>
                <td><input type="text" class="form-control" placeholder="Remarks" /></td>
              </tr>
              <tr>
                <td>James Tom</td>
                <td><input type="text" class="form-control" placeholder="Marks" /></td>
                <td><input type="text" class="form-control" placeholder="Remarks" /></td>
              </tr>
            </table>
          </div>
          <button class="btn btn-primary">Save</button>
        </div>
      </div>

      <div class="panel panel-primary animated fadeInUp hide-me2">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary">Clear All Scores</button>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
              <tr>
                <th data-toggle="true">Student Name</th>
                <th>Marks</th>
                <th data-hide="phone" data-sort-ignore="true">Remarks</th>
                <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
              </tr>
              </thead>
              <tr>
                <td>Yansen Nafir</td>
                <td>56</td>
                <td>Keen in his studies. Can improve more</td>
                <td>
                  <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
