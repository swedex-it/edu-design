<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Al Quds | Dashboard</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<!-- Bootstrap 3.3.4 -->
	<link href="../dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
	<!-- Theme style -->
	<link rel="stylesheet" href="../dist/css/dashboard.css" />
	<!-- bootstrap style override style -->
	<link rel="stylesheet" href="../dist/css/EDU.css"/>
	<!-- default theme -->
	<link href="../dist/css/skins/skin-blue-light.css" rel="stylesheet" type="text/css" />
	<!-- full calender css -->
	<link rel="stylesheet" href="../dist/css/fullcalendar.css" />
	<!-- DataTables css -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/dt-1.10.8/datatables.min.css"/>
	<!-- Animate css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.4.0/animate.min.css" />
	<!-- iCheck custom checbox -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.1/skins/all.css" />
	<!-- custom dropdown functionalities -->
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
	<!-- bootstrap wysihtml5 - text editor -->
  <link href="../dist/css/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
	<!-- Footable css file -->
	<link rel="stylesheet" href="../dist/css/footable.core.min.css" />
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="skin-blue-light sidebar-mini">
<?php include "_modals.php"; ?>
