<!-- Doing this way since modal has to be loaded right from the Dashboard Sidebar -->

<!-- select batch to set grading levels modal form -->
<div class="modal fade" id="modalSelectBatchGradingLevels">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Select Batch to Set Grading Levels</h4>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-primary">
							<div class="panel-heading" role="tab" id="headingOne">
								<h2 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Grade 1
										<small>2 batches</small>
									</a>
								</h2>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade1">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="setgradinglevels.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="setgradinglevels.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Grade 2
										<small>2 Batches</small>
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade2">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="setgradinglevels.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="setgradinglevels.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading" id="headingThree" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								<h4 class="panel-title">
									<a>
										Grade 3
										<small>2 Batches</small>
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade3">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="setgradinglevels.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="setgradinglevels.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- select garde and batch to set exam modal form -->
<div class="modal fade" id="modalExamByGradeBatch">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Select Batch to Setup Exams</h4>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
						<div class="panel panel-primary">
							<div class="panel-heading" role="tab" id="head1">
								<h2 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion2" href="#c-egb1" aria-expanded="true" aria-controls="c-egb1">
										Grade 1
										<small>2 batches</small>
									</a>
								</h2>
							</div>
							<div id="c-egb1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="head1">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade1">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="exambygrade-batch.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="exambygrade-batch.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading" role="tab" id="head2">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#c-egb2" aria-expanded="false" aria-controls="c-egb2">
										Grade 2
										<small>2 Batches</small>
									</a>
								</h4>
							</div>
							<div id="c-egb2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="head2">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade2">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="exambygrade-batch.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="exambygrade-batch.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading" id="head3" role="button" data-toggle="collapse" data-parent="#accordion2" href="#c-egb3" aria-expanded="false" aria-controls="c-egb3">
								<h4 class="panel-title">
									<a>
										Grade 3
										<small>2 Batches</small>
									</a>
								</h4>
							</div>
							<div id="c-egb3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="head3">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade3">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="exambygrade-batch.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="exambygrade-batch.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- select batch to set timetable modal form -->
<div class="modal fade" id="modalSelectBatchForTimetable">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Select Batch to Setup Exams</h4>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
						<div class="panel panel-primary">
							<div class="panel-heading" role="tab" id="head1-1">
								<h2 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion3" href="#t-stt1" aria-expanded="true" aria-controls="c-egb1">
										Grade 1
										<small>2 batches</small>
									</a>
								</h2>
							</div>
							<div id="t-stt1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="head1-1">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade1">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="settimetable.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="settimetable.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading" role="tab" id="head2-2">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#t-stt2" aria-expanded="false" aria-controls="c-egb2">
										Grade 2
										<small>2 Batches</small>
									</a>
								</h4>
							</div>
							<div id="t-stt2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="head2-2">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade2">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="settimetable.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="settimetable.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading" id="head3-3" role="button" data-toggle="collapse" data-parent="#accordion3" href="#t-stt3" aria-expanded="false" aria-controls="c-egb3">
								<h4 class="panel-title">
									<a>
										Grade 3
										<small>2 Batches</small>
									</a>
								</h4>
							</div>
							<div id="t-stt3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="head3-3">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade3">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="settimetable.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="settimetable.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- select batch to manage batch modal form -->
<div class="modal fade" id="modalSelectBatchForManageBatch">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Select Batch to Setup Exams</h4>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
						<div class="panel panel-primary">
							<div class="panel-heading" role="tab" id="head1-1">
								<h2 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion3" href="#t-stt1" aria-expanded="true" aria-controls="c-egb1">
										Grade 1
										<small>2 batches</small>
									</a>
								</h2>
							</div>
							<div id="t-stt1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="head1-1">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade1">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="managebatch.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="managebatch.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading" role="tab" id="head2-2">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#t-stt2" aria-expanded="false" aria-controls="c-egb2">
										Grade 2
										<small>2 Batches</small>
									</a>
								</h4>
							</div>
							<div id="t-stt2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="head2-2">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade2">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="managebatch.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="managebatch.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading" id="head3-3" role="button" data-toggle="collapse" data-parent="#accordion3" href="#t-stt3" aria-expanded="false" aria-controls="c-egb3">
								<h4 class="panel-title">
									<a>
										Grade 3
										<small>2 Batches</small>
									</a>
								</h4>
							</div>
							<div id="t-stt3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="head3-3">
								<div class="panel-body">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered" id="grade3">
												<tr>
													<th>Batch Name</th>
													<th>No. of Students</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
												<tr>
													<td><a href="managebatch.php">A</a></td>
													<td>23</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
												<tr>
													<td><a href="managebatch.php">B</a></td>
													<td>17</td>
													<td>1-Jan-2015</td>
													<td>31-Dec-2015</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
