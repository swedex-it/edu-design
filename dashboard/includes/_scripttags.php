<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="../dist/js/vendors/jquery/jquery-1.11.2.min.js"></script>
<!-- Bootstrap 3.3.5 JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- Full Calendar JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.2/fullcalendar.min.js"></script>
<!--Dropzone custom js file -->
<!-- <script src="../dist/js/vendors/dropzone/dropzone.js"></script> -->
<!-- DataTables Js -->
<script type="text/javascript" src="https://cdn.datatables.net/r/bs/dt-1.10.8/datatables.min.js"></script>
<!-- iCheck Js file experiement -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.1/icheck.min.js"></script>
<!-- select2 custom dropdown Js -->
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../dist/js/vendors/wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- slimScroll for fixed layout -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.6/jquery.slimscroll.min.js"></script>
<!-- Footable Js File -->
<script src="../dist/js/vendors/footable/footable.all.min.js"></script>
<!-- EDU App -->
<script src="../dist/js/app.js" type="text/javascript"></script>

</body>
</html>
