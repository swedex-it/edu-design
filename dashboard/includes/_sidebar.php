<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

  <!-- Sidebar user panel (optional) -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="../dist/images/user2-160x160.jpg" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
      <p>Hisham Haniffa</p>
      <!-- Status -->
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>

  <!-- search form (Optional) -->
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search..."/>
      <span class="input-group-btn">
        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
      </span>
    </div>
  </form>
  <!-- /.search form -->

  <!-- Sidebar Menu -->
  <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="active"><a href="index.php"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
    <li class="treeview">
      <a href="#"><i class='fa fa-user'></i> <span>Student</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="addstudent.php">New Student</a></li>
        <li><a href="viewstudent.php">View Student</a></li>
        <li><a href="studentcategory.php">Manage Student Category</a></li>
        <li><a href="manageguardian.php">Manage Parent Details</a></li>
        <li><a href="online-reg.php">Online Registration</a></li>
        <li><a href="student-approval.php">Student Approval</a></li>
        <li><a href="waitinglist.php">Waiting List</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class='fa fa-users'></i> <span>Staff</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="addstaff.php">New Staff</a></li>
        <li><a href="viewstaff.php">View Staff</a></li>
        <li><a href="leavetypes.php">Leave Types</a></li>
        <li><a href="leave-requests.php">Leave Request <span class="label label-primary pull-right">34</span></a></li>
        <li><a href="subjectassociation.php">Subject Association</a></li>
        <li><a href="managestaffcat.php">Manage Staff Categories</a></li>
        <li><a href="managestaffpos.php">Manage Positions</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class='fa fa-book'></i> <span>Grade</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="addgrade.php">Add Grade</a></li>
        <li><a href="viewgrade.php">View Grade</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class='fa fa-file-text'></i> <span>Exam</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="#" data-toggle="modal" data-target="#modalSelectBatchGradingLevels">Set Grading Levels</a></li>
        <li><a href="#" data-toggle="modal" data-target="#modalExamByGradeBatch">Select Grade-Batch</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class='fa fa-calendar'></i> <span>Timetable</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="#" data-toggle="modal" data-target="#modalSelectBatchForTimetable">View Timetable</a></li>
        <li><a href="fulltimetable.php">View all Timetables</a></li>
        <li><a href="#" data-toggle="modal" data-target="#modalSelectBatchForTimetable">Set Timetable</a></li>
        <li><a href="setweekdays.php">Set Weekdays</a></li>
        <li><a href="setclasstime.php">Set Class Timing</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class='fa fa-money'></i> <span>Fees</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="feecollection.php">All Fees</a></li>
        <li><a href="collectfee.php">Collect Fees</a></li>
        <li><a href="paidstudents.php">Paid Students</a></li>
        <li><a href="unpaidstudents.php">Unpaid Students</a></li>
        <!-- <li><a href="#">All Students</a></li> -->
        <li><a href="feecategory.php">Fee Categories</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class='fa fa-files-o'></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="advancedreport.php">Advanced Report</a></li>
        <li><a href="studentexamreport.php">Student Exam Report</a></li>
        <li><a href="batchexamreport.php">Batch Assessment Report</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class='fa fa-cog'></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="manageacademicyears.php">Manage Academic Year</a></li>
        <li><a href="createnewuser.php">Create New User</a></li>
        <li><a href="manageusers.php">Manage Users</a></li>
        <li><a href="schoolsettings.php">School Settings</a></li>
        <li><a href="news.php">News</a></li>
        <li><a href="activityfeed.php">Activity Feed</a></li>
        <li><a href="newacademicyear.php">Academic Year</a></li>
        <li><a href="modules.php">Modules</a></li>
        <li><a href="createuserrole.php">User Roles</a></li>
        <li><a href="studentcategory.php">Manage Student Category</a></li>
        <li><a href="onlineapplicantsettings.php">Online Applicant Settings</a></li>
        <li><a href="studentdocsettings.php">Student Doc Settings</a></li>
      </ul>
    </li>
    <li><a href="import.php"><i class='fa fa-download'></i> <span>Import</span></a></li>
    <li><a href="export.php"><i class='fa fa-upload'></i> <span>Export</span></a></li>
    <li><a href="mailbox.php"><i class='fa fa-comments-o'></i> <span>Notify</span></a></li>
  </ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
