<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  <div class="pull-right hidden-xs">
    Al Quds Powered by EDU
  </div>
  <!-- Default to the left -->
  <strong>Copyright &copy; 2015 <a href="#">Al Quds</a>.</strong> All rights reserved.
</footer>
