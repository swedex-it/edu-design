<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Activity Feed
        <small>Track Activities</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">Activity Feed</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-body">
          <form class="form-inline">
            <div class="col-xs-12 col-md-3">
              <div class="form-group">
                <label for="feeType" class="control-label">Feed Type</label>
                <select id="feeType" class="form-control">
                  <option>--Select Feed--</option>
                  <option>Login</option>
                  <option>Logout</option>
                  <option>Student activation</option>
                  <option>Student Creation</option>
                  <option>Exam Creation</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-md-4">
              <div class="form-group">
                <label for="startDate" class="control-label">Start Date</label>
                <input type="date" id="startDate" class="form-control" placeholder="Start Date"/>
              </div>
            </div>
            <div class="col-xs-12 col-md-3">
              <div class="form-group">
                <label for="endDate" class="control-label">End Date</label>
                <input type="date" id="endDate" class="form-control" placeholder="End Date"/>
              </div>
            </div>
            <div class="col-xs-12 col-md-2">
              <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Find" />
              </div>
            </div>
          </form>
        </div>
      </div>

      <ul class="timeline">

        <!-- timeline time label -->
        <li class="time-label">
          <span class="bg-red">
            11 Oct. 2015
          </span>
        </li>
        <!-- /.timeline-label -->

        <!-- timeline item -->
        <li>
          <!-- timeline icon -->
          <i class="fa fa-arrow-circle-o-right"></i>
          <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
            <h3 class="timeline-header"><a href="#">Hisham Haniffa - Admin</a> </h3>
            <div class="timeline-body">
              logged into the system at 07:37 AM - Sep 11.2015.
            </div>
          </div>
        </li>
        <li>
          <!-- timeline icon -->
          <i class="fa fa-arrow-circle-o-right"></i>
          <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
            <h3 class="timeline-header"><a href="#">Hisham Haniffa - Admin</a> </h3>
            <div class="timeline-body">
              logged into the system at 07:37 AM - Sep 11.2015.
            </div>
          </div>
        </li>
        <li>
          <!-- timeline icon -->
          <i class="fa fa-arrow-circle-o-right"></i>
          <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
            <h3 class="timeline-header"><a href="#">Hisham Haniffa - Admin</a> </h3>
            <div class="timeline-body">
              logged into the system at 07:37 AM - Sep 11.2015.
            </div>
          </div>
        </li>
      </ul>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
