<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Fee Particulars
        <small>Create and manage fee particular</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Fees</li>
        <li class="active">Fee Categories</li>
        <li class="active">Fee Particulars</li>
      </ol>
    </section>
    <!-- Modal forms here -->

    <div class="modal fade" id="FeeParticular">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">New Fee Particular</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group required">
                <label for="feeParticularName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="feeParticularName" placeholder="Particular Name">
                </div>
              </div>
              <div class="form-group">
                <label for="feeParticularDescription" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                  <textarea class="form-control" placeholder="Description" id="feeParticularDescription"></textarea>
                </div>
              </div>
              <!-- use multi select dropdown -->
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div>
                    <label>
                      <input type="radio" name="catSel"> All
                    </label>
                  </div>
                  <div onclick="$('#feeStudentCat').css('display','none');$('#feeAdminNo').css('display','block')">
                    <label>
                      <input type="radio" name="catSel"> Admission No {will show Admission No input field}
                    </label>
                  </div>
                  <div  onclick="$('#feeAdminNo').css('display','none');$('#feeStudentCat').css('display','block')">
                    <label>
                      <input type="radio" name="catSel"> Student Category {will show all student category in dropdown}
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group required hide-me" id="feeAdminNo">
                <label for="feeParticularAdmissionNo" class="col-sm-2 control-label">Admission No</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="feeParticularAdmissionNo" placeholder="Admission No">
                </div>
              </div>
              <div class="form-group required hide-me" id="feeStudentCat">
                <label for="feeParticularStudentCat" class="col-sm-2 control-label">Student Category</label>
                <div class="col-sm-10">
                  <select id="feeParticularStudentCat" class="form-control">
                    <option>Orphan</option>
                    <option>Half Payment</option>
                    <option>Full Payment</option>
                  </select>
                </div>
              </div>
              <div class="form-group required">
                <label for="feeParticularAmount" class="col-sm-2 control-label">Amount</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="feeParticularAmount" placeholder="Particular Name">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- End of Modal forms -->

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#FeeParticular" >New Fee Particular</button>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th data-toggle="true">Name</th>
                  <th data-hide="phone,tablet">Description</th>
                  <th>Amount</th>
                  <th>Fee Category</th>
                  <th data-hide="phone,tablet">Category</th>
                  <th data-hide="phone,tablet">Admission No</th>
                  <th data-hide="phone,tablet">Action</th>
                </tr>
              </thead>
              <tr>
                <td>Full Payment</td>
                <td>Full payment for general students</td>
                <td>25000</td>
                <td>First Term Fees</td>
                <td>General</td>
                <td> - </td>
                <td>
                  <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#FeeParticular"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
              </tr>
              <tr>
                <td>Half Payment</td>
                <td>Half payment for schol students</td>
                <td>25000</td>
                <td>First Term Fees</td>
                <td>Scholarship</td>
                <td> - </td>
                <td>
                  <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#FeeParticular"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
