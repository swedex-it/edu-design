<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Fee Categories
        <small>Create and manage fee categories</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Fees</li>
        <li class="active">Fee Categories</li>
      </ol>
    </section>
    <!-- Modal forms here -->

    <div class="modal fade" id="FeeCategory">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">New Fee Category</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group required">
                <label for="feeCategoryName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="feeCategoryName" placeholder="Category Name">
                </div>
              </div>
              <div class="form-group">
                <label for="feeCategoryDescription" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                  <textarea class="form-control" placeholder="Description"></textarea>
                </div>
              </div>
              <!-- use multi select dropdown -->
              <div class="form-group">
                <label class="col-sm-2 control-label">Select Grade</label>
                <div class="col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> All
                    </label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> Grade 1
                    </label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> Grade 2
                    </label>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- End of Modal forms -->

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#FeeCategory" >New Fee Category</button>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th data-toggle="true">Name</th>
                  <th data-hide="phone,tablet">Description</th>
                  <th>Batch</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                  <th data-sort-ignore="true">Manage</th>
                </tr>
              </thead>
              <tr>
                <td>Exam Fees</td>
                <td>1st term examination fees</td>
                <td>A</td>
                <td>
                  <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#FeeCategory"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
                <td>
                  <a class="btn btn-primary btn-sm" href="feeparticular.php"><span class="glyphicon glyphicon-list-alt"></span> Add Particular</a>
                </td>
              </tr>
              <tr>
                <td>First Term Fees</td>
                <td>1st term fees</td>
                <td>A</td>
                <td>
                  <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
                <td>
                  <button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-list-alt"></span> Add Particular</button>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
