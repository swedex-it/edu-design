<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        School Settings
        <small>Configure school setup</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">School Settings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <form class="form-horizontal">
        <div class="panel panel-primary">
          <div class="panel-heading">
            School Information
          </div>
          <div class="panel-body">
            <div class="col-xs-12 col-md-6">
              <div class="form-group required">
                <label for="schoolName" class="col-sm-3 control-label">School Name</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="schoolName" placeholder="School Name">
                </div>
              </div>
              <div class="form-group">
                <label for="foundedOn" class="col-sm-3 control-label">Founded On</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" id="foundedOn" placeholder="Founded On">
                </div>
              </div>
              <div class="form-group required">
                <label for="address" class="col-sm-3 control-label">Address</label>
                <div class="col-sm-9">
                  <textarea class="form-control" id="address" placeholder="Founded On"></textarea>
                </div>
              </div>
              <div class="form-group required">
                <label for="phone" class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="phone" placeholder="Founded On" />
                </div>
              </div>
              <div class="form-group required">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                  <input type="email" class="form-control" id="email" placeholder="Email" />
                </div>
              </div>
            </div>

            <div class="col-xs-12 col-md-6">
              <div class="form-group required">
                <label for="regId" class="col-sm-3 control-label">Registration ID</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="regId" placeholder="Registration ID" />
                </div>
              </div>
              <div class="form-group required">
                <label for="zipCode" class="col-sm-3 control-label">Zip Code</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="zipCode" placeholder="Zip Code" />
                </div>
              </div>
              <div class="form-group required">
                <label for="alternatePhone" class="col-sm-3 control-label">Alternate Phone</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="alternatePhone" placeholder="Alternate Phone" />
                </div>
              </div>
              <div class="form-group required">
                <label for="logo" class="col-sm-3 control-label">Upload Logo</label>
                <div class="col-sm-9">
                  <input type="file" class="form-control" id="logo" />
                  <small>supported formats: .jpg, .png</small>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-primary">
          <div class="panel-heading">
            Principal / Head of the Institution
          </div>
          <div class="panel-body">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label for="principalName" class="col-sm-3 control-label">Name of Principal</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="principalName" placeholder="Principal's Name" />
                </div>
              </div>
              <div class="form-group">
                <label for="principalPhone" class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="principalPhone" placeholder="Phone" />
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label for="principalEmail" class="col-sm-3 control-label">Email of Principal</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="principalEmail" placeholder="Principal's Email" />
                </div>
              </div>
              <div class="form-group">
                <label for="principalMobile" class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="principalMobile" placeholder="Mobile" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-primary">
          <div class="panel-heading">
            School Year Setup
          </div>
          <div class="panel-body">
            <div class="col-xs-12 col-md-6">
              <div class="form-group required">
                <label for="selectAcademicYear" class="col-sm-3 control-label">Academic Year</label>
                <div class="col-sm-9">
                  <select class="form-control" id="selectAcademicYear">
                    <option>AY2015</option>
                    <option>AY2016</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="financialYearStart" class="col-sm-3 control-label">Finance Year Start</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" id="financialYearStart"  />
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                  <a href="" class="btn btn-primary btn-sm">Create new Academic Year</a>
                </div>
              </div>
              <div class="form-group">
                <label for="financialYearEnd" class="col-sm-3 control-label">Finance Year End</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" id="financialYearEnd"  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-primary">
          <div class="panel-heading">
            Application Settings
          </div>
          <div class="panel-body">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label for="currencyCode" class="col-sm-3 control-label">Currency Code</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="currencyCode" placeholder="Currency Code" />
                </div>
              </div>
              <div class="form-group">
                <label for="dateFormat" class="col-sm-3 control-label">Date Format</label>
                <div class="col-sm-9">
                  <select class="form-control" id="dateFormat">
                    <option>DD-MM-YYYY</option>
                    <option>YYYY-MM-DD</option>
                    <option>DDD-MM-YYYY</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label for="language" class="col-sm-3 control-label">Time Zone</label>
                <div class="col-sm-9">
                  <select class="form-control" id="language">
                    <option>English</option>
                    <option>Spanish</option>
                    <option>Franch</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="timeFormat" class="col-sm-3 control-label">Time Format</label>
                <div class="col-sm-9">
                  <select class="form-control" id="timeFormat">
                    <option>12 hours Format</option>
                    <option>24 hours Format</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-12 text-center">
            <button class="btn btn-primary btn-lg">Apply</button>
          </div>
        </div>
      </form>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
