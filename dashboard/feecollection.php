<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Fee Collection
        <small>Create and manage fee collections</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Fees</li>
        <li class="active">Fee Collection</li>
      </ol>
    </section>
    <!-- Modal forms here -->

    <div class="modal fade" id="FeeCollection">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">New Fee Collection</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group required">
                <label for="feeCategory" class="col-sm-2 control-label">Fee Category</label>
                <div class="col-sm-10">
                  <select id="feeCategory" class="form-control">
                    <option>Fee category name + Batch</option>
                    <option>1st Term-A</option>
                    <option>1st Term-B</option>
                  </select>
                </div>
              </div>
              <div class="form-group required">
                <label for="feeCollectionName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="feeCollectionName" placeholder="Collection Name">
                </div>
              </div>
              <div class="form-group required">
                <label for="startDate" class="col-sm-2 control-label">Start Date</label>
                <div class="col-sm-10">
                  <input type="date" class="form-control" id="startDate" placeholder="Start Date" />
                </div>
              </div>
              <div class="form-group required">
                <label for="endDate" class="col-sm-2 control-label">End Date</label>
                <div class="col-sm-10">
                  <input type="date" class="form-control"  id="endDate" placeholder="End Date" />
                </div>
              </div>
              <div class="form-group required">
                <label for="dueDate" class="col-sm-2 control-label">Due Date</label>
                <div class="col-sm-10">
                  <input type="date" class="form-control"  id="dueDate" placeholder="Due Date" />
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- End of Modal forms -->

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#FeeCollection" >New Fee Collection</button>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th data-toggle="true">Name</th>
                  <th data-hide="phone">Start Date</th>
                  <th data-hide="phone">End Date</th>
                  <th data-hide="phone">Due Date</th>
                  <th data-hide="phone,tablet">Fee Category</th>
                  <th data-hide="phone,tablet">Batch</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                </tr>
              </thead>
              <tr>
                <td>First Term</td>
                <td>Sep 09 2015</td>
                <td>Dec 10 2015</td>
                <td>Dec 5 2015</td>
                <td>1st Term</td>
                <td>A</td>
                <td>
                  <button class="btn btn-warning btn-sm" onclick="alert('Cannot edit fee collection because its already begun, delete or create new one')"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
              </tr>
              <tr>
                <td>Second Term</td>
                <td>Jan 05 2015</td>
                <td>May 10 2015</td>
                <td>May 15 2015</td>
                <td>2nd Term</td>
                <td>A</td>
                <td>
                  <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#FeeCollection"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
