<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Staff Categories
        <small>All staff categories</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Staff</li>
        <li class="active">Manage Staff Categories</li>
      </ol>
    </section>
    <!-- Modal forms Here-->
    <div class="modal fade" id="editStaffCat">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Category</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="categoryName" >Name</label>
                <input type="text" class="form-control" placeholder="Category Name" />
              </div>
              <div class="form-group">
                <label for="categoryPrefix" >Prefix</label>
                <input type="text" class="form-control" placeholder="Prefix" />
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="newStaffCat">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create New Staff Category</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="categoryName">Name</label>
                <input type="text" class="form-control" placeholder="Category Name" />
              </div>
              <div class="form-group">
                <label for="categoryPrefix" >Prefix</label>
                <input type="text" class="form-control" placeholder="Prefix" />
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End of Modal forms -->

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#newStaffCat">Add Category</button>
          </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="panel-body">
          <div class="row">
            <p class="col-xs-12 col-md-4">
              <input type="text" id="filter" class="form-control" placeholder="Search.."/>
            </p>
          </div>
          <div class="table-responsive">
            <table class="table foo table-bordered"  data-filter=#filter data-page-navigation=".pagination">
              <thead>
                <tr>
                  <th data-toggle="true">Name</th>
                  <th data-hide="phone,tablet">Prefix</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Academic</td>
                  <td>AC</td>
                  <td>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editStaffCat"><i class="ion-edit"></i> Edit</button>
                    <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><i class="ion-trash-a"></i> Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>Administrative</td>
                  <td>AD</td>
                  <td>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editStaffCat"><i class="ion-edit"></i> Edit</button>
                    <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><i class="ion-trash-a"></i> Delete</button>
                  </td>
                </tr>
                <tr>
                  <td>Teacher</td>
                  <td>TR</td>
                  <td>
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editStaffCat"><i class="ion-edit"></i> Edit</button>
                    <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure ?')"><i class="ion-trash-a"></i> Delete</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
