<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student
        <small>Add new student</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">New student</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-offset-2 col-md-8">
          <div class="col-md-3">
            <button type="button" class="btn btn-primary btn-nav">New Student</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Parent</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Documents</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Preview</button>
          </div>
        </div>
      </div>
      <br />
      <div class="row">
        <div class="col-md-12">
          <form>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Personal Details</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="admissionno">Admission No</label>
                      <input type="text" class="form-control" id="admissionno" placeholder="Admission No">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="admissiondate">Admission Date</label><br>
                      <input type="date" class="form-control date" id="admissiondate">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="firstname">First Name</label>
                      <input type="text" class="form-control" id="firstname" placeholder="First Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="middlename">Middle Name</label>
                      <input type="text" class="form-control" id="middlename" placeholder="Middle Name">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastname">Last Name</label>
                      <input type="text" class="form-control" id="lastname" placeholder="Last Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="batch">Batch</label>
                      <select class="form-control" id="batch">
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="dateofbirth">Date of Birth</label>
                      <input type="date" class="form-control date" id="dateofbirth" >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="gender">Gender</label>
                      <select class="form-control" id="gender">
                        <option>Male</option>
                        <option>Female</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="bloodgroup">Blood Group</label>
                      <select class="form-control" id="bloodgroup">
                        <option></option>
                        <option>AB+</option>
                        <option>AB-</option>
                        <option>A+</option>
                        <option>A-</option>
                        <option>B+</option>
                        <option>B-</option>
                        <option>O+</option>
                        <option>O-</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="birthplace">Birth Place</label>
                      <input type="text" class="form-control" id="birthplace" placeholder="Birth Place">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="nationality">Nationality</label>
                      <input type="text" class="form-control" id="nationality" placeholder="Nationality">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="language">Language</label>
                      <input type="text" class="form-control" id="language" placeholder="Language">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="religion">Religion</label>
                      <select class="form-control" id="religion">
                        <option>Islam</option>
                        <option>Christian</option>
                        <option>Buddhist</option>
                        <option>Hindu</option>
                        <option>Other</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="category">Student Category</label>
                      <select class="form-control" id="category">
                        <option>Full Payment</option>
                        <option>Half Payment</option>
                        <option>Orphan</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Contact Details</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="address1">Address Line 1</label>
                      <input type="text" class="form-control" id="address1" placeholder="Address Line 1">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="address2">Address Line 2</label>
                      <input type="text" class="form-control" id="address2" placeholder="Address Line 2">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="city">City</label>
                      <input type="text" class="form-control" id="city" placeholder="City">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="zip">Zip Code</label>
                      <input type="text" class="form-control" id="zip" placeholder="Zip Code">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="country">Country</label>
                      <input type="text" class="form-control" id="country" placeholder="Country">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="phone1">Phone 1</label>
                      <input type="text" class="form-control" id="phone1" placeholder="Phone 1">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="phone2">Phone 2</label>
                      <input type="text" class="form-control" id="phone2" placeholder="Phone 2">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="email">Email Address</label>
                      <input type="text" class="form-control" id="email" placeholder="Email Address">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Additional Information</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="photo">Photo Upload</label>
                      <input type="file" class="form-control" id="photo">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="institute">Previous Institute</label>
                      <input type="text" class="form-control" id="institute" placeholder="Previous Institute">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="year">Year</label>
                      <input type="text" class="form-control" id="year" placeholder="Year">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="grade">Grade</label>
                      <input type="text" class="form-control" id="grade" placeholder="Grade">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-offset-5 col-md-2">
                <a href="parent.php" class="btn btn-primary btn-lg">Parent <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php include "includes/_footer.php"; ?>

<?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
