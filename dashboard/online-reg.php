<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Online Registration
        <small>Find your online applicants here</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">Online Registration</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Your Page Content Here -->
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="panel-title">John Smith</div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-4">
              <table>
                <tr>
                  <td>Applicant ID:</td>
                  <td>32156</td>
                </tr>
                <tr>
                  <td>Phone:</td>
                  <td>+94 772658978</td>
                </tr>
                <tr>
                  <td>Email:</td>
                  <td>someone@somewhere.com</td>
                </tr>
              </table>

            </div>
            <div class="col-md-2">
              08:46 am, 02 Aug 2015
              <h3><span class="label label-lg label-primary">Pending</span></h3>

            </div>
            <div class="col-md-6">
              <div class="btn-group btn-justified">
                <button type="button" class="btn btn-primary btn-sm">
                  <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span> Details
                </button>
                <button type="button" class="btn btn-success btn-sm">
                  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Approve
                </button>
                <button type="button" class="btn btn-danger btn-sm">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                </button>
              </div>
            </div>

          </div>
        </div>
      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="panel-title">Ahmad Yahya</div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-4">
              <table>
                <tr>
                  <td>Applicant ID:</td>
                  <td>5656</td>
                </tr>
                <tr>
                  <td>Phone:</td>
                  <td>+94 772658978</td>
                </tr>
                <tr>
                  <td>Email:</td>
                  <td>ahmad@yahya.com</td>
                </tr>
              </table>

            </div>
            <div class="col-md-2">
              12:46 pm, 24 Sep 2015
              <h3><span class="label label-success">Approved</span></h3>

            </div>
            <div class="col-md-6">
              <div class="btn-group btn-justified">
                <button type="button" class="btn btn-primary btn-sm">
                  <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span> Details
                </button>
                <button type="button" class="btn btn-danger btn-sm">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
