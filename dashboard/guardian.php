<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student
        <small>Add new student</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">New student</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-offset-2 col-md-8">
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">New Student</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-primary btn-nav" >Parent</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Documents</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Preview</button>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="row">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Existing Parent?</h3>
              </div>
              <div class="panel-body">
                <form class="form">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="oparent">Enter Parent Name</label>
                        <input type="text" class="form-control" id="oparent" placeholder="Enter parent name">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <button type="submit" class="btn btn-default">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <form>
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">New Parent</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="text" class="form-control" id="firstname" placeholder="First Name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input type="text" class="form-control" id="lastname" placeholder="Last Name">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="relation">Relation</label>
                        <input type="text" class="form-control" id="relation" placeholder="Relation">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="dateofbirth">Date of Birth</label>
                        <input type="date" class="form-control date" id="dateofbirth">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="Education">Education</label>
                        <input type="text" class="form-control" id="education" placeholder="Education">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="occupation">Occupation</label>
                        <input type="text" class="form-control" id="occupation" placeholder="Occupation">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="income">Income</label>
                        <input type="text" class="form-control" id="income" placeholder="Income">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" placeholder="Email">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="phone">Mobile Phone</label>
                        <input type="text" class="form-control" id="phone" placeholder="Mobile Phone">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="officephone">Office Phone</label>
                        <input type="text" class="form-control" id="officephone" placeholder="Office Phone">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" placeholder="City">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" class="form-control" id="country" placeholder="Country">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2 col-md-offset-5">
                  <!-- <input type="submit" class="btn-primary" value="DOCUMENTS  >" > -->
                  <a href="documents.php" class="btn btn-primary btn-lg">Document <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
