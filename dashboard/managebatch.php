<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Batch
        <small>Manage batches</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Grade</li>
        <li class="active">Manage Grade and Batches</li>
        <li class="active">Manage Batch</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalSelectBatchForManageBatch">Change Batch</button>
          </div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-12 col-md-5">
              <p class="tpad">
                <strong>Grade / Batch:</strong> 1 / A
              </p>
              <p>
                <strong>Class Teacher:</strong> Humaiz Azad
              </p>
            </div>
            <div class="col-xs-12 col-md-5">
              <div class="btn-group btn-group-justified" role="group" aria-label="...">
                <div class="btn-group" role="group">
                  <button type="button" class="btn btn-primary"><h4>Students</h4>250</button>
                </div>
                <div class="btn-group" role="group">
                  <button type="button" class="btn btn-primary"><h4>Subjects</h4>6</button>
                </div>
                <div class="btn-group" role="group">
                  <button type="button" class="btn btn-primary"><h4>Staffs</h4>1</button>
                </div>
                <div class="btn-group" role="group">
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <h4>Action</h4>
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Add Student</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-list-alt"></span> New Subject</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-pencil"></span> Assign Teacher</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-check"></span> Promote Batch</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-ban-circle"></span> Deactivate Batch</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="row tpad">
            <div class="col-xs-12">
              <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#students" aria-controls="students" role="tab" data-toggle="tab">Students</a></li>
                  <li role="presentation"><a href="#subjects" aria-controls="subjects" role="tab" data-toggle="tab">Subjects</a></li>
                  <li role="presentation"><a href="#timetable" aria-controls="timetable" role="tab" data-toggle="tab">Timetable</a></li>
                  <li role="presentation"><a href="#exams" aria-controls="exams" role="tab" data-toggle="tab">Exams</a></li>
                  <li role="presentation"><a href="#waitingListStudents" aria-controls="waitingListStudents" role="tab" data-toggle="tab">Waiting List Students</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="students">
                    <div class="col-xs-12 col-md-12 text-right tpad">
                      <a class="btn btn-primary" href="addstudent.php">Add Students</a>
                    </div>
                    <div class="col-xs-12 tpad">
                      <div class="table-responsive">
                        <table class="table foo table-bordered">
                          <thead>
                            <tr>
                              <th data-toggle="true">Student Name</th>
                              <th>Admission No</th>
                              <th data-hide="phone,tablet">Gender</th>
                              <th data-hide="phone">Status</th>
                              <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                            </tr>
                          </thead>
                          <tr>
                            <td>Humaiz Azad</td>
                            <td>8</td>
                            <td>Male</td>
                            <td>In Progress</td>
                            <td>
                              <button class="btn btn-primary btn-sm">Make Inactive</button>
                            </td>
                          </tr>
                          <tr>
                            <td>Muhammad Muhsin</td>
                            <td>9</td>
                            <td>Male</td>
                            <td>In Progress</td>
                            <td>
                              <button class="btn btn-primary btn-sm">Make Inactive</button>
                            </td>
                          </tr>
                          <tr>
                            <td>Abdul Raheem</td>
                            <td>10</td>
                            <td>Male</td>
                            <td>In Progress</td>
                            <td>
                              <button class="btn btn-primary btn-sm">Make Inactive</button>
                            </td>
                          </tr>
                        </table>
                      </div>
                      <div class="table-responsive">
                        <div class="callout callout-info">
                          <h4>Inactive Students</h4>
                          <p>No Inactive Students In This Batch</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="subjects">
                    <div class="panel panel-primary">
                      <div class="panel-heading">
                        <div class="text-right">
                          <button class="btn btn-primary">Add Subjects to Batch</button>
                        </div>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                          <table class="table foo table-bordered">
                            <thead>
                              <tr>
                                <th data-toggle="true">Name</th>
                                <th>Code</th>
                                <th data-hide="phone">Max Weekly Classes</th>
                                <th data-sort-ignore="true" data-hide="phone,tablet">Action</th>
                              </tr>
                            </thead>
                            <tr>
                              <td>Maths</td>
                              <td>M</td>
                              <td>15</td>
                              <td>
                                <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div role="tabpanel" class="tab-pane fade" id="timetable">
                    <div class="panel panel-primary">
                      <div class="panel-heading">
                        <div class="pull-left">
                          <button class="btn btn-success">Generate PDF</button>
                        </div>
                        <div class="text-right">
                          <a class="btn btn-primary" href="setweekdays.php">Set Weekdays</a>
                          <a class="btn btn-primary" href="setclasstime.php">Set Class Timing</a>
                        </div>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                          <table class="table foo table-bordered">
                            <thead>
                              <tr>
                                <th data-toggle="true">Weekdays</th>
                                <th data-hide="phone,tablet" data-sort-ignore="true">08:30 AM - 10: 00 AM</th>
                                <th data-hide="phone,tablet" data-sort-ignore="true">10:00 AM - 12: 00 PM</th>
                                <th data-hide="phone,tablet" data-sort-ignore="true">01:00 PM - 02: 00 PM</th>
                                <th data-hide="phone,tablet" data-sort-ignore="true">02:00 PM - 03: 00 PM</th>
                              </tr>
                            </thead>
                            <tr>
                              <td>Monday</td>
                              <td>Maths <a href="#" onclick="confirm('Are you sure ?');"><i class="ion-ios-close"></i></a></td>
                              <td>English <a href="#" onclick="confirm('Are you sure ?');"> <i class="ion-ios-close"></i></a></td>
                              <td>Science <a href="#" onclick="confirm('Are you sure ?');"> <i class="ion-ios-close"></i></a></td>
                              <td>Economics <a href="#" onclick="confirm('Are you sure ?');"> <i class="ion-ios-close"></i></a></td>
                            </tr>
                            <tr>
                              <td>Tuesday</td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                            </tr>
                            <tr>
                              <td>Wednesday</td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                            </tr>
                            <tr>
                              <td>Thursday</td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                            </tr>
                            <tr>
                              <td>Friday</td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                              <td><a href="#">Assign</a></td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div role="tabpanel" class="tab-pane fade" id="exams">
                    <div class="panel panel-primary">
                      <div class="panel-heading">
                        <div class="text-right">
                          <button class="btn btn-primary" data-toggle="modal" data-target="#createNewExam">Create Exam</button>
                          <a class="btn btn-primary" href="setgradinglevels.php">Grading Levels</a>
                        </div>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive" id="exams">
                          <table class="table foo table-bordered">
                            <thead>
                              <tr>
                                <th data-toggle="true">Name</th>
                                <th>Exam Type</th>
                                <th data-hide="phone,tablet">Date Is Published</th>
                                <th data-hide="phone,tablet">Result Is Published</th>
                                <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                                <th data-hide="phone" data-sort-ignore="true">Manage</th>
                              </tr>
                            </thead>
                            <tr>
                              <td>First Term</td>
                              <td>Marks and Grade</td>
                              <td>Yes</td>
                              <td>No</td>
                              <td>
                                <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#createNewExam"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure you want to delete ?')"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                              </td>
                              <td>
                                <button class="btn btn-primary btn-sm" onclick="$('.hide-me').css('display','block');"><span class="glyphicon glyphicon-list-alt"></span> Manage Exam</button>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="waitingListStudents">
                    <div class="tpad">
                      <div class="callout callout-info">
                        <h4>Waiting List</h4>
                        <p>No Waiting List Students In This Batch</p>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
