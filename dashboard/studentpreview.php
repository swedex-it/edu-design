<?php include "includes/_header.php"; ?>
<div class="wrapper">

	<?php include "includes/_nav.php"; ?>

	<aside class="main-sidebar">
		<?php include "includes/_sidebar.php"; ?>
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Student
				<small>Add new student</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
				<li class="active">Student</li>
				<li class="active">Preview</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row hidden-sm hidden-xs bpad">
				<div class="col-md-offset-2 col-md-8">
					<div class="col-md-3">
						<button type="button" class="btn btn-default btn-nav" disabled="disabled">New Student</button>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-default btn-nav" disabled="disabled">Parent</button>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-default btn-nav" disabled="disabled">Documents</button>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary btn-nav">Preview</button>
					</div>
				</div>
			</div>
			<!-- Start of Tabs -->
			<ul id="Profile" class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#student" id="student-tab" role="tab" data-toggle="tab" aria-controls="student" aria-expanded="true">Student</a></li>
				<li role="presentation" class=""><a href="#fees" role="tab" id="fees-tab" data-toggle="tab" aria-controls="fees" aria-expanded="false">Fees</a></li>
				<li role="presentation" class=""><a href="#documents" role="tab" id="documents-tab" data-toggle="tab" aria-controls="documents" aria-expanded="false">Documents</a></li>
				<li role="presentation" class=""><a href="#assessment" role="tab" id="assessment-tab" data-toggle="tab" aria-controls="assessment" aria-expanded="false">Assessments</a></li>
			</ul>
			<div id="ProfileContent" class="tab-content">
				<div role="tabpanel" class="tab-pane active fade in" id="student" aria-labelledby="student-tab"><!-- student tab-->
					<div class="panel"><br>
						<div class="row">
							<div class="col-md-3 text-center">
								<div class="panel panel-default">
									<div class="panel-body">
										<img src="../dist/images/user2-160x160.jpg" alt="profilepic" class="centered-align img-responsive img-thumbnail">
										<div class="muted">Humaiz Azad</div>
										<div>humaiz89@gmail.com</div>
										<div>Grade: 3-B</div>
										<div>Adm No: QD1</div>
									</div>
								</div>
							</div>
							<div class="col-md-9">
								<div class="box box-primary">
									<div class="box-header">
										<h3 class="box-title">
											Student Profile: <strong>Humaiz Azad</strong>
										</h3>
										<div class="box-tools pull-right">
											<a href="addstudent.php" class="btn btn-primary btn-sm">Edit</a>
										</div>
									</div>
								</div>
								<div class="panel panel-primary">
									<div class="panel-heading">Personal Details</div>
									<div class="panel-body">
										<div class="table-responsive col-sm-6">
											<table class="table">
												<tr>
													<td class="text-left">Admission No</td>
													<td class="text-right">QD1</td>
												</tr>

												<tr>
													<td class="text-left">First Name</td>
													<td class="text-right">Humaiz</td>
												</tr>

												<tr>
													<td class="text-left">Middle Name</td>
													<td class="text-right">Abdullah</td>
												</tr>

												<tr>
													<td class="text-left">Birth Place</td>
													<td class="text-right">Galle</td>
												</tr>
												<tr>
													<td class="text-left">Nationality</td>
													<td class="text-right">Sri Lankan</td>
												</tr>
												<tr>
													<td class="text-left">Blood Group</td>
													<td class="text-right">B +</td>
												</tr>
												<tr>
													<td class="text-left">Category</td>
													<td class="text-right">Full Payment</td>
												</tr>
											</table>
										</div>
										<div class="table-responsive">
											<table class="table">
												<tr>
													<td class="text-left">Admission Date</td>
													<td class="text-right">2015-10-05</td>
												</tr>
												<tr>
													<td class="text-left">Last Name</td>
													<td class="text-right">Azad</td>
												</tr>
												<tr>
													<td class="text-left">Date of Birth</td>
													<td class="text-right">1995-10-04</td>
												</tr>
												<tr>
													<td class="text-left">Gender</td>
													<td class="text-right">Male</td>
												</tr>
												<tr>
													<td class="text-left">Language</td>
													<td class="text-right">English</td>
												</tr>
												<tr>
													<td class="text-left">Religion</td>
													<td class="text-right">Islam</td>
												</tr>
												<tr>
													<td class="text-left">Batch</td>
													<td class="text-right">5</td>
												</tr>
											</table>
										</div>
									</div>
								</div>

								<div class="panel panel-primary">
									<div class="panel-heading">Contact Details</div>
									<div class="panel-body">
										<div class="table-responsive col-sm-6">
											<table class="table">
												<tr>
													<td class="text-left">Address Line 1</td>
													<td class="text-right">199 New Moors Street</td>
												</tr>
												<tr>
													<td class="text-left">City</td>
													<td class="text-right">Colombo</td>
												</tr>
												<tr>
													<td class="text-left">Mobile</td>
													<td class="text-right">+94 77 082 2678</td>
												</tr>
												<tr>
													<td class="text-left">Email</td>
													<td class="text-right">humaiz89@gmail.com</td>
												</tr>
											</table>
										</div>
										<div class="table-responsive col-sm-6">
											<table class="table">
												<tr>
													<td class="text-left">Address Line 2</td>
													<td class="text-right">Colombo 12</td>
												</tr>
												<tr>
													<td class="text-left">Country</td>
													<td class="text-right">Sri Lanka</td>
												</tr>

												<tr>
													<td class="text-left">Landline</td>
													<td class="text-right">112432608</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- end of student tab-->
				<div role="tabpanel" class="tab-pane fade " id="fees" aria-labelledby="fees-tab"><!-- fees tab-->
					<div class="panel"><br>
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h2 class="panel-title">Pending Fees</h2>
									</div>
									<div class="panel-body">
										No Pending Fees
									</div>
								</div>

								<div class="panel panel-primary">
									<div class="panel-heading">
										<h2 class="panel-title">Paid Fees</h2>
									</div>
									<div class="panel-body">
										<table class="table-responsive col-sm-6">
											<thead>
												<tr>
													<td>Category</td>
													<td>Amount</td>
													<td>Amount Paid</td>
													<td>Balance</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end of fees tab-->
				<div role="tabpanel" class="tab-pane fade" id="documents" aria-labelledby="documents-tab"> <!-- documents tab-->
					
					<div class="panel"><br>
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h2 class="panel-title">Document Name</h2>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-4">Photo</div>
											<div class="col-md-4">Approved</div>
											<div class="col-md-2"><a class="btn btn-default">Delete</a></div>
											<div class="col-md-2"><a class="btn btn-default">Download</a></div>
										</div>
									</div>
								</div>
								<br>
								<div class="panel">
									<form>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="name">Document Name*</label>
													<input type="text" name="name" class="form-control" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="file">Choose File</label>
													<input type="file" name="file" class="form-control" />
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-2">
												<button class="btn btn-default">Add Another</button>
											</div>
											<div class="col-md-3">
												<button class="btn btn-default">Save</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div><!-- end of documents tab-->
				<div role="tabpanel" class="tab-pane fade" id="assessment" aria-labelledby="assessment-tab"> <!-- assessment tab-->
					<div class="panel"><br>
						<div class="row">
						<div class="col-md-2"></div>
							<div class="col-md-8">
								<table class="table table-responsive col-sm-6">
									<thead>
										<tr>
											<td>Exam Group Name</td>
											<td>Subject</td>
											<td>Score</td>
											<td>Result</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- end of assessment tab-->
			</div>
			<!-- End of Tabs -->
		</section><!-- /.content -->
	</div><!-- /.content-wrapper -->

	<?php include "includes/_footer.php"; ?>

	<?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
