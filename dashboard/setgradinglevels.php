<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Exam
        <small>Set Grading Levels</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Exam</li>
        <li class="active">Set Grading Levels</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading" id="grade-heading">
              <div class="pull-left text-left tpad">
                Grade: 10 Batch: A
              </div>
              <div class="text-right">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalStudentCategory">New Grading Level</button>
              </div>
            </div>
            <div class="panel-body">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table foo table-bordered">
                    <thead>
                      <tr>
                        <th data-toggle="true">Grade Name</th>
                        <th>Mininmum Mark</th>
                        <th data-hide="phone" data-sort-ignore="true">Actions</th>
                      </tr>
                    </thead>
                    <tr>
                      <td>
                        A
                      </td>
                      <td>
                        80
                      </td>
                      <td>
                        <button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        B
                      </td>
                      <td>
                        70
                      </td>
                      <td>
                        <button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        C
                      </td>
                      <td>
                        60
                      </td>
                      <td>
                        <button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        D
                      </td>
                      <td>
                        50
                      </td>
                      <td>
                        <button type="button" class="btn btn-warning btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button type="button" class="btn btn-danger btn-sm" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
