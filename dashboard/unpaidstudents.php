<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Collect Fees
        <small>Fee collection</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Fee</li>
        <li class="active">Collect Fees</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="pull-left">
            Search Filter
          </div>
          <div class="text-right">
            <button class="btn btn-success">Generate PDF</button>
          </div>
        </div>
        <div class="panel-body">
          <form class="form-inline">
            <div class="row">
              <label for="batch" class="col-sm-1 text-right">Batch</label>
              <div class="col-sm-2">
                <select class="form-control staff-grade" id="grade">
                  <option>--Select Batch--</option>
                  <option>A</option>
                  <option>B</option>
                  <option>C</option>
                  <option>D</option>
                </select>
              </div>
              <label for="feeCollectionCat" class="col-sm-2 text-right">Fee Collection</label>
              <div class="col-sm-2">
                <select class="form-control staff-subject" id="feeCollectionCat" onclick="$('.table-responsive').css('display','block');">
                  <option>--Select Collection--</option>
                  <option>First Term</option>
                  <option>Second Term</option>
                  <option>Third Term</option>
                  <option>Special Exam</option>
                </select>
              </div>
              <label for="dueDate" class="col-sm-2 text-right">Due Date</label>
              <label for="dueDate" class="col-sm-2">Dec 10 2015</label>
            </div>
          </form>
          <hr />
          <div class="table-responsive hide-me animated fadeInUp">
            <table class="table foo table-bordered table-condensed">
              <thead>
                <tr class="info">
                  <th data-toggle="true">Particulars</th>
                  <th>Applicable For</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tr class="info">
                <td>Full Payment</td>
                <td>General</td>
                <td>25,000 Rs</td>
              </tr>
              <tr class="info">
                <td>Half Payment</td>
                <td>Scholarship</td>
                <td>12,500 Rs</td>
              </tr>
            </table>
          </div>
          <div class="table-responsive hide-me animated fadeInUp">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th data-toggle="true">Admission No</th>
                  <th>Student Name</th>
                  <th data-hide="phone">Fees</th>
                  <th data-hide="phone">Fees Paid</th>
                  <th data-hide="phone,tablet">Balance</th>
                  <th data-hide="phone,tablet" data-sort-ignore="true">Payment</th>
                </tr>
              </thead>
              <tr>
                <td>8</td>
                <td>Humaiz Azad</td>
                <td>25,000 Rs</td>
                <td>0 Rs</td>
                <td>0 Rs</td>
                <td><a href="#">Full</a> | <a href="#">Partial</a></td>
              </tr>
              <tr>
                <td>9</td>
                <td>Zaid Zakir</td>
                <td>25,000 Rs</td>
                <td>20,000 Rs</td>
                <td>5,000 Rs</td>
                <td><a href="#">Full</a> | <a href="#">Partial</a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
