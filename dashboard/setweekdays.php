<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Set Weekdays
        <small>Weekdays for the batch</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">Set Weekdays</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <a class="btn btn-primary btn-small" href="settimetable.php">Timetable</a>
          </div>
        </div>
        <div class="panel-body">
          <form class="setWeekDayForm">
            <div class="checkbox">
              <label><input type="checkbox"> <strong>Monday</strong></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> <strong>Tuesday</strong></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> <strong>Wednesday</strong></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> <strong>Thursday</strong></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> <strong>Friday</strong></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox"> <strong>Saturday</strong></label>
            </div>
            <div class="checkbox">
              <label><input type="checkbox" > <strong>Sunday</strong></label>
            </div>
          </form>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
<script type="text/javascript">

$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square',
    increaseArea: '20%' // optional
  });
});

</script>
