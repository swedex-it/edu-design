<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
        <small>Manage your account</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">Online Registration</li>
      </ol>
    </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-success bg-green">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="../dist/images/user2-160x160.jpg" alt="User profile picture">
                  <h3 class="profile-username text-center">Hisham Haniffa</h3>
                  <p class="text-muted text-center">Software Engineer</p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#manageProfile" data-toggle="tab">Manage Profile</a></li>
                  <li><a href="#changePassword" data-toggle="tab">Change Password</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="manageProfile">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="username" placeholder="Name" value="Hisham Haniffa" disabled>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" placeholder="Email" value="hishamhaniffa4@gmail.com">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="firstName" class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="firstName" placeholder="First Name" value="Hisham">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="lastName" class="col-sm-2 control-label">Last Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="lastName" placeholder="Last Name" value="Haniffa">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="photo" class="col-sm-2 control-label">Photo</label>
                        <div class="col-sm-10">
                          <img src="../dist/images/user2-160x160.jpg" class="profile-user-img-edit img-responsive img-circle" />
                          <input type="file" class="form-control" id="photo" placeholder="Profile Pic">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="changePassword">
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label for="oldPassword" class="col-sm-2 control-label">Old Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="oldPassword" placeholder="Old Password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="newPassword" class="col-sm-2 control-label">New Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="newPassword" placeholder="New Password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="newPasswordConfirm" class="col-sm-2 control-label">Retype Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="newPasswordConfirm" placeholder="New Password">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

        <?php include "includes/_footer.php"; ?>

        <?php include "includes/_rightsidebar.php"; ?>
      </div><!-- ./wrapper -->
      <?php include "includes/_scripttags.php"; ?>
