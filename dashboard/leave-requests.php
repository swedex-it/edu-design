<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Leave Request
        <small>Manage staff leave requests</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Staff</li>
        <li class="active">Leave Request</li>
      </ol>
    </section>
    <div class="modal fade" id="editLeaveRequestModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Leave Request</h4>
          </div>
          <div class="modal-body">
            <h3>Hisham Haniffa</h3>
            <div><b>Leave Type:</b> Festival</div>
            <div><b>Start Date:</b> 10 Oct 2015</div>
            <div><b>End Date:</b> 15 Oct 2015</div>
            <p>
              <b>Additional Message:</b><br />
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua.
            </p>
          </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-primary" href="leave-requests.php">Approve</a>
            <a type="button" class="btn btn-danger" href="leave-requests.php">Reject</a>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-10">
                  <div class="row lpad">
                    <div class="col-md-12">
                      <div class="leave-title"><i class="fa fa-file"></i> <a href="#" data-toggle="modal" data-target="#editLeaveRequestModal">Muhammad Muhsin</a></div>
                    </div>
                    <div class="col-md-12">
                      <div><b>Leave Type:</b> Sick</div>
                      <div><b>Applied Date:</b> 01 Sep 2015</div>
                    </div>
                  </div>
                </div>
                <div class="col-md-2 text-center">
                  <i class="ion-checkmark-circled fa-3x leave-approved"></i><br />
                  Approved
                </div>
              </div>
            </div>
          </div>
          <div class="box box-success">
            <div class="box-body">
              <div class="row">
                <div class="col-md-10">
                  <div class="row lpad">
                    <div class="col-md-12">
                      <div class="leave-title"><i class="fa fa-file"></i> <a href="#" data-toggle="modal" data-target="#editLeaveRequestModal">Humaiz Azad</a></div>
                    </div>
                    <div class="col-md-12">
                      <div><b>Leave Type:</b> Sick</div>
                      <div><b>Applied Date:</b> 01 Oct 2015</div>
                    </div>
                  </div>
                </div>
                <div class="col-md-2 text-center">
                  <i class="ion-checkmark-circled fa-3x leave-approved"></i><br />
                  Approved
                </div>
              </div>
            </div>
          </div>
          <div class="box box-danger">
            <div class="box-body">
              <div class="row">
                <div class="col-md-10">
                  <div class="row lpad">
                    <div class="col-md-12">
                      <div class="leave-title"><i class="fa fa-file"></i> <a href="#" data-toggle="modal" data-target="#editLeaveRequestModal">Kasib Kismath</a></div>
                    </div>
                    <div class="col-md-12">
                      <div><b>Leave Type:</b> Festival</div>
                      <div><b>Applied Date:</b> 10 Oct 2015</div>
                    </div>
                  </div>
                </div>
                <div class="col-md-2 text-center">
                  <i class="ion-close-circled fa-3x leave-declined"></i><br />
                  Rejected
                </div>
              </div>
            </div>
          </div>
          <div class="box box-warning">
            <div class="box-body">
              <div class="row">
                <div class="col-md-10">
                  <div class="row lpad">
                    <div class="col-md-12">
                      <div class="leave-title"><i class="fa fa-file"></i> <a href="#" data-toggle="modal" data-target="#editLeaveRequestModal">James Ken</a></div>
                    </div>
                    <div class="col-md-12">
                      <div><b>Leave Type:</b> Festival</div>
                      <div><b>Applied Date:</b> 10 Oct 2015</div>
                    </div>
                  </div>
                </div>
                <div class="col-md-2 text-center">
                  <i class="ion-android-sync fa-3x leave-pending"></i><br />
                  Pending
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
