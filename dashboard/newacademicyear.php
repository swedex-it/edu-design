<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        New Academic Year
        <small>Add and update details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">New Academic Year</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Current Academic Year: <strong>AY2015</strong>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-12">
              <label>Starts On: <strong>Jan 01 2015</strong></label>
            </div>
            <div class="col-sm-12">
              <label>Ends On: <strong>Dec 30 2015</strong></label>
            </div>
            <div class="col-sm-12">
              <label>Description: <strong>Academic Year 2015</strong></label>
            </div>
          </div>
        </div>
      </div>

      <form class="form-horizontal">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Enter Upcoming Academic Year Details
        </div>
        <div class="panel-body">

            <div class="form-group required">
              <label for="academicName" class="col-sm-1 col-sm-offset-1 control-label">Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="academicName" placeholder="Name">
              </div>
            </div>
            <div class="form-group required">
              <label for="academicStartDate" class="col-sm-1 col-sm-offset-1 control-label">Starts On</label>
              <div class="col-sm-4">
                <input type="date" class="form-control" id="academicStartDate" placeholder="Starts On">
              </div>
              <label for="academicEndDate" class="col-sm-1 control-label">Ends On</label>
              <div class="col-sm-4">
                <input type="date" class="form-control" id="academicEndDate" placeholder="Ends On">
              </div>
            </div>
            <div class="form-group required">
              <label for="academicDesc" class="col-sm-1 col-sm-offset-1 control-label">Description</label>
              <div class="col-sm-9">
                <textarea class="form-control" id="academicDesc" placeholder="Description"></textarea>
              </div>
            </div>
            <div class="form-group required">
              <label for="academicStatus" class="col-sm-1 col-sm-offset-1 control-label">Status</label>
              <div class="col-sm-9">
                <select class="form-control" id="academicStatus">
                  <option>Active</option>
                  <option>Inactive</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="previousAcademicStruct" class="control-label col-sm-5 col-sm-offset-1">Do you want to import previous academic structure?</label>
              <div class="col-sm-5" id="previousAcademicStruct">
                <div class="tpad">
                  <label>
                    <input type="radio" onclick="$('#prevAcademicStrut').css('display','block')" name="prevAca"> Yes
                  </label>&nbsp;
                  <label>
                    <input type="radio" onclick="$('#prevAcademicStrut').css('display','none')" name="prevAca"> No
                  </label>
                </div>
              </div>
            </div>
        </div>
      </div>

      <div class="panel panel-primary hide-me" id="prevAcademicStrut">
        <div class="panel-heading">
          Select the academic year and choose the details to be imported
        </div>
        <div class="panel-body">
          <div class="form-group">
            <label for="selectAcademicYear" class="control-label col-sm-1 col-sm-offset-1">Academic Year</label>
            <div class="col-sm-9">
              <select id="selectAcademicYear" class="form-control">
                <option>AY2015</option>
                <option>AY2016</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <ul>
              <li><input type="radio" name="lvl1" />Course structure only</li>
              <li>
                <input type="radio" name="lvl1" onclick="$('#lvl2').attr('disabled',false)"/>Course and Batch structure
                <ul>
                  <li>
                    <input type="checkbox" name="lvl2" id="lvl2" onclick="$('.lvl3').attr('disabled',false)" disabled/> Subject structure
                    <ul>
                      <li><input type="checkbox" name="" class="lvl3" disabled/> Subject - Teacher  Association</li>
                      <li><input type="checkbox" name="" class="lvl3" disabled/> Timetable structure</li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="form-group text-center">
        <button class="btn btn-primary btn-lg">Save</button>
      </div>
      </form>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
