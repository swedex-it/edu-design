<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Set Class Timing
        <small>Class timing for batch</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Timetable</li>
        <li class="active">Set Class Timing</li>
      </ol>
    </section>

    <!-- Modal form here -->

    <div class="modal fade" id="createClassTiming">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Set New Class Timing</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group required">
                <label for="name" class="control-label">Name</label>
                <input type="text" id="name" class="form-control" placeholder="Timing Name"/>
              </div>
              <div class="form-group required">
                <label for="startTime" class="control-label">Start Time</label>
                <input type="text" id="startTime" class="form-control" placeholder="Start Time"/>
              </div>
              <div class="form-group required">
                <label for="endTime" class="control-label">End Time</label>
                <input type="text" id="endTime" class="form-control" placeholder="End Time"/>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Is Break
                </label>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- End of Modal form here -->


    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="pull-left">
            <button class="btn btn-success" data-toggle="modal" data-target="#createClassTiming">Create Class Timing</button>
          </div>
          <div class="text-right">
            <a class="btn btn-primary" href="settimetable.php">Timetable</a>
          </div>
        </div>
        <div class="panel-body">
          <div class="box box-primary">
            <div class="box-header">
              <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-2">
                  <strong>Grade <label class="label label-primary">Class 10</label></strong>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 hidden-xs">
                  <strong>Batch <label class="label label-primary">A</label></strong>
                </div>
                <!-- only will be visible on xs screen size -->
                <div class="col-xs-12 visible-xs-block tpad">
                  <strong>Batch <label class="label label-primary">A</label></strong>
                </div>
              </div>
              <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <button class="btn btn-default" data-toggle="modal" data-target="#modalExamByGradeBatch">Change Batch</button>
              </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table foo table-bordred">
                  <thead>
                    <tr>
                      <th data-toggle="true">Name</th>
                      <th >Start Time</th>
                      <th >End Time</th>
                      <th data-hide="phone" data-sort-ignore="true">Is Break</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                    </tr>
                  </thead>
                  <tr>
                    <td>1'st Period</td>
                    <td>10:00 AM</td>
                    <td>10:45 AM</td>
                    <td>No</td>
                    <td>
                      <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                      <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                  </tr>
                </table>
              </div>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
