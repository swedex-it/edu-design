<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Settings
        <small>Create New Role</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">Create New Role</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          Assign Role
        </div>
        <div class="box-body">
          <form>
            <div class="col-xs-12 col-md-4">
              <div class="form-group">
                <select class="form-control">
                  <option>Select Role</option>
                  <option>Admin</option>
                  <option>Staff</option>
                  <option>Student</option>
                </select>
              </div>
              <button class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Create New Role</h3>
        </div>
        <div class="panel-body">
          <form class="form-horizontal">
            Some modules are interrelated
            <div class="form-group required">
              <label for="name" class="control-label col-xs-3 col-md-1">Name</label>
              <div class="col-xs-6 col-md-3">
                  <input type="text" class="form-control" name="name" id="name"/>
              </div>
            </div>
            <div class="form-group required">
              <label for="description" class="control-label col-xs-3 col-md-1">Description</label>
              <div class="col-xs-6 col-md-3">
                <input type="text" class="form-control" name="description" id="description"/>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Select All</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Home</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Students</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Employees</label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Grades</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Exam</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Timetable</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Fees</label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Reports</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Settings</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Import</label>
                </div>
              </div>
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Export</label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-md-3">
                <div class="checkbox">
                  <label><input type="checkbox"> Notify</label>
                </div>
              </div>
            </div>
            <div class="row tpad">
              <div class="col-xs-4 col-md-2">
                <input type="submit" class="btn btn-primary" value="Create" />
              </div>
            </div>

          </form>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
<script type="text/javascript">

$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square',
    increaseArea: '20%' // optional
  });
});

</script>
