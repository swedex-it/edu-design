<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Waiting List
        <small>Manage your waiting list</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">Waiting List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-default">
        <div class="panel-body"> <!-- Panel Body -->
          <div class="row">
           <p class="col-xs-12 col-md-4">
             <input type="text" id="filter" class="form-control" placeholder="Search.."/>
           </p>
         </div>
         <div class="table-responsive">
           <table class="table foo table-bordered text-center" data-filter=#filter data-page-navigation=".pagination">
             <thead>
              <tr>
                <th data-toggle="true">Name</th>
                <th>Grade / Batch</th>
                <th>Priority</th>
                <th data-hide="phone" data-sort-ignore="true">Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Yansen Yasir</td>
                <td>1 A</td>
                <td>1</td>
                <td>
                  <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                  <a href="#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span> Approve</a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="row">
         <div class="col-xs-12 text-center">
          <ul class="pagination"></ul>
        </div>
      </div>
    </div> <!-- /Panel Body -->
  </div>
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php include "includes/_footer.php"; ?>

<?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
