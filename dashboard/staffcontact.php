<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Staff
        <small>Add new staff</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Staff</li>
        <li class="active">New Staff</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-offset-4 col-md-8">
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Staff Details</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-primary btn-nav">Staff Contact Details</button>
          </div>
        </div>
      </div>
      <br />
      <div class="row">
        <div class="col-md-12">
          <form>
            <div  class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Contact Details</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="addressLine1">Address Line 1</label>
                      <input type="text" class="form-control" id="addressLine1" placeholder="Address Line 1">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="addressLine2">Address Line 2</label><br>
                      <input type="text" class="form-control date" id="addressLine2" placeholder="Address Line 2">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="city">City</label>
                      <input type="text" class="form-control" id="city" placeholder="City">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="country">Country</label>
                      <select class="form-control" id="country">
                        <option>--Select Country--</option>
                        <option>Sri Lanka</option>
                        <option>India</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="zipCode">Zip Code</label>
                      <input type="text" class="form-control" id="zipCode" placeholder="Zip Code">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="text" class="form-control date" id="email" placeholder="Email">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="homePhone">Home Phone</label>
                      <input type="text" class="form-control date" id="homePhone" placeholder="Home Phone" >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="mobilePhone">Mobile Phone</label>
                      <input type="text" class="form-control date" id="mobilePhone" placeholder="Mobile Phone">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-md-offset-5">
                <!-- <input type="submit" class="btn-primary" value="DOCUMENTS  >" > -->
                <a href="staffcontact.php" class="btn btn-primary btn-lg">Save <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
