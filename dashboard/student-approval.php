<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="modal fade" id="waitingListMang">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Waiting List Management</h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <p class="form-control-static">Yansen Yasir
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Grade</label>
                    <div class="col-sm-10">
                      <select class="form-control">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="priority" class="col-sm-2 control-label">Priority</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="priority" placeholder="Priority">
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a type="button" class="btn btn-primary" href="waitinglist.php">Apply</a>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <h1>
          Online Registration Approval
          <small>Find your online applicants here</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
          <li class="active">Student</li>
          <li class="active">Online Registration Approval</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Your Page Content Here -->
        <div class="panel panel-primary">
          <div class="panel-heading">
            <div class="panel-title">John Smith</div>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <table>
                  <tr>
                    <td>Applicant ID:</td>
                    <td>32156</td>
                  </tr>
                  <tr>
                    <td>Phone:</td>
                    <td>+94 772658978</td>
                  </tr>
                  <tr>
                    <td>Email:</td>
                    <td>someone@somewhere.com</td>
                  </tr>
                </table>

              </div>
              <div class="col-md-2">
                08:46 am, 02 Aug 2015
                <h3><span class="label label-lg label-primary">Pending</span></h3>

              </div>
              <div class="col-md-6">
                <div class="btn-group btn-justified">
                  <button type="button" class="btn btn-primary btn-sm">
                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span> Details
                  </button>
                  <button type="button" class="btn btn-success btn-sm">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Approve
                  </button>
                  <button type="button" class="btn btn-danger btn-sm">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                  </button>
                  <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#waitingListMang">
                    <span class="glyphicon glyphicon-hourglass" aria-hidden="true"></span> Waiting List
                  </button>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <div class="panel-title">Ahmad Yahya</div>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-4">
                <table>
                  <tr>
                    <td>Applicant ID:</td>
                    <td>5656</td>
                  </tr>
                  <tr>
                    <td>Phone:</td>
                    <td>+94 772658978</td>
                  </tr>
                  <tr>
                    <td>Email:</td>
                    <td>ahmad@yahya.com</td>
                  </tr>
                </table>

              </div>
              <div class="col-md-2">
                12:46 pm, 24 Sep 2015
                <h3><span class="label label-success">Approved</span></h3>

              </div>
              <div class="col-md-6">
                <div class="btn-group btn-justified">
                  <button type="button" class="btn btn-primary btn-sm">
                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span> Details
                  </button>
                  <button type="button" class="btn btn-danger btn-sm">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <?php include "includes/_footer.php"; ?>

    <?php include "includes/_rightsidebar.php"; ?>
  </div><!-- ./wrapper -->
  <?php include "includes/_scripttags.php"; ?>
