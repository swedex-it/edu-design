<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Grade and Batches
        <small>All grades and batches details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Grade</li>
        <li class="active">Manage Grade and Batches</li>
      </ol>
    </section>
    <!-- Modal forms Here -->
    <div class="modal fade" id="editGrade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Grade</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group required">
                <label for="editGrade" class=" col-sm-2 control-label">Grade</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Grade" value="10"/>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="addBatch">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Batch</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="form-group required">
                <label for="addGrade" class=" col-sm-2 control-label">Grade</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="addGrade" placeholder="Grade" />
                </div>
              </div>
              <div class="form-group required">
                <label for="addYear" class=" col-sm-2 control-label">Year</label>
                <div class="col-sm-10">
                  <select class="form-control" id="addYear">
                    <option>--Select Year--</option>
                    <option>2015</option>
                    <option>2016</option>
                    <option>2017</option>
                  </select>
                </div>
              </div>
              <div class="form-group required">
                <label for="addTeacher" class=" col-sm-2 control-label">Teacher</label>
                <div class="col-sm-10">
                  <select class="form-control" id="addTeacher">
                    <option>--Select Teacher--</option>
                    <option>Rajesh Aneja</option>
                    <option>Charles Timmothy</option>
                    <option>Jamie Ken</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End of Modal forms Here -->

    <!-- Main content -->
    <section class="content">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading bg-green" role="tab" id="head1">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#c-vg1" aria-expanded="true" aria-controls="collapseOne">
              Class 10 <br />
              <small> 4 - Batch(es)</small>
            </a>
            <div class="text-right pull-up">
              <div class="btn-group" role="group" aria-label="actionButtons">
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editGrade">Edit</button>
                <button type="button" class="btn btn-danger" onclick="confirm('Are you sure you want to delete ?')">Delete</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBatch">Add Batch</button>
                <button type="button" class="btn btn-primary" data-toggle="collapse" data-parent="#accordion" href="#c-vg1"><i class="ion-arrow-down-b"></i></button>
              </div>
            </div>
          </div>
          <div id="c-vg1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body"> <!-- Panel Body -->
              <div class="table-responsive">
                <table class="table foo table-bordered">
                  <thead>
                    <tr>
                      <th data-toggle="true">Class</th>
                      <th>Class Teacher</th>
                      <th data-hide="phone,tablet">Year</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><a href="managebatch.php">A</a></td>
                      <td>-</td>
                      <td>2015</td>
                      <td>
                        <button class="btn btn-warning btn-sm">Edit</button>
                        <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure you want to delete ?')">Delete</button>
                        <a class="btn btn-primary btn-sm" href="addstudent.php">Add Student</a>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="managebatch.php">B</a></td>
                      <td>-</td>
                      <td>2015</td>
                      <td>
                        <button class="btn btn-warning btn-sm">Edit</button>
                        <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure you want to delete ?')">Delete</button>
                        <a class="btn btn-primary btn-sm" href="addstudent.php">Add Student</a>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="managebatch.php">C</a></td>
                      <td>Rajash Aneja</td>
                      <td>2015</td>
                      <td>
                        <button class="btn btn-warning btn-sm">Edit</button>
                        <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure you want to delete ?')">Delete</button>
                        <button class="btn btn-primary btn-sm">Add Student</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div> <!-- End panel body -->
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading bg-green" role="tab" id="head2">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#c-vg2" aria-expanded="false" aria-controls="collapseTwo">
              Class 11 <br />
              <small> 5 - Batch(es)</small>
            </a>
            <div class="text-right pull-up">
              <div class="btn-group" role="group" aria-label="actionButtons">
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editGrade">Edit</button>
                <button type="button" class="btn btn-danger" onclick="confirm('Are you sure you want to delete ?')">Delete</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBatch">Add Batch</button>
                <button type="button" class="btn btn-primary" data-toggle="collapse" data-parent="#accordion" href="#c-vg2"><i class="ion-arrow-down-b"></i></button>
              </div>
            </div>
          </div>
          <div id="c-vg2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table foo table-bordered">
                  <thead>
                    <tr>
                      <th data-toggle="true">Class</th>
                      <th>Class Teacher</th>
                      <th data-hide="phone,tablet">Year</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><a href="managebatch.php">A</a></td>
                      <td>-</td>
                      <td>2015</td>
                      <td>
                        <button class="btn btn-warning btn-sm">Edit</button>
                        <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure you want to delete ?')">Delete</button>
                        <a class="btn btn-primary btn-sm" href="addstudent.php">Add Student</a>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="managebatch.php">B</a></td>
                      <td>-</td>
                      <td>2015</td>
                      <td>
                        <button class="btn btn-warning btn-sm">Edit</button>
                        <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure you want to delete ?')">Delete</button>
                        <a class="btn btn-primary btn-sm" href="addstudent.php">Add Student</a>
                      </td>
                    </tr>
                    <tr>
                      <td><a href="managebatch.php">C</a></td>
                      <td>Rajash Aneja</td>
                      <td>2015</td>
                      <td>
                        <button class="btn btn-warning btn-sm">Edit</button>
                        <button class="btn btn-danger btn-sm" onclick="confirm('Are you sure you want to delete ?')">Delete</button>
                        <button class="btn btn-primary btn-sm">Add Student</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
<script type="text/javascript">
$(document).ready(function() {
    $('.collapse').on('shown.bs.collapse', function (e) {
        //prevents re-size from happening before tab shown
        // e.preventDefault();
        $('.table.foo').trigger('footable_initialize');
        // //show tab panel
        // $(this).collapse('show');
        // //fire re-size of footable
        $('.table.foo').trigger('footable_resize');
    });
});
</script>
