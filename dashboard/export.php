<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Import CSV
        <small>Find your online applicants here</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">Online Registration</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <form class="form-horizontal">
            <div class="form-group required">
              <label for="modal" class="control-label col-md-1">Model</label>
              <div class="col-xs-12 col-md-5">
                <select id="modal" class="form-control">
                  <option>Students</option>
                  <option>Staffs</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-1 col-md-5">
                <input type="submit" class="btn btn-primary btn-lg" value="Next"/>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
