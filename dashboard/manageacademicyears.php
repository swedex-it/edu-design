<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Settings
        <small>Academic Years</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">Academic Years</li>
      </ol>
    </section>

    <!-- Modal forms here -->

    <!-- Modal form ends here -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading" id="grade-heading">
              <div class="pull-left">
                <h3 class="panel-title tpad"> Manage Academic Years </h3>
              </div>
              <div class="text-right">
                <a class="btn btn-primary" href="newacademicyear.php">Add Academic Years</a>
              </div>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table foo table-bordered">
                  <thead>
                    <tr>
                      <th data-toggle="true">Name</th>
                      <th>Starts On</th>
                      <th>Ends On</th>
                      <th data-hide="phone,tablet">Description</th>
                      <th data-hide="phone">Status</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>AY2014</td>
                      <td>Jan 1, 2014</td>
                      <td>Dec 1, 2014</td>
                      <td>Academic Year 2015</td>
                      <td>Active</td>
                      <td>
                        <button type="button" class="btn btn-sm btn-warning" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button type="button" class="btn btn-sm btn-danger" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                    <tr>
                      <td>AY2014</td>
                      <td>Jan 1, 2014</td>
                      <td>Dec 1, 2014</td>
                      <td>Academic Year 2015</td>
                      <td>Active</td>
                      <td>
                        <button type="button" class="btn btn-sm btn-warning" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button type="button" class="btn btn-sm btn-danger" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                    <tr>
                      <td>AY2014</td>
                      <td>Jan 1, 2014</td>
                      <td>Dec 1, 2014</td>
                      <td>Academic Year 2015</td>
                      <td>Active</td>
                      <td>
                        <button type="button" class="btn btn-sm btn-warning" id="${user.student.studentID}" ><span class="glyphicon glyphicon-edit"></span> Edit</button>
                        <button type="button" class="btn btn-sm btn-danger" id="${user.student.studentID}" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
