<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Modules
        <small>Manage Modules</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">Modules</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table foo table-bordered">
              <thead>
                <tr>
                  <th>Name</th>
                  <th data-sort-ignore="true">Action</th>
                </tr>
              </thead>
              <tr>
                <td>Student</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Staff</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Grade</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Exam</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Timetable</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Fees</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Reports</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Settings</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Import</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Export</td>
                <td><a href="#">Disable</a></td>
              </tr>
              <tr>
                <td>Notify</td>
                <td><a href="#">Disable</a></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
