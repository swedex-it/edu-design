<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Exam Report By Batch
        <small>Batch wise exam report</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Report</li>
        <li class="active">Exam Report By Batch</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          Search Filter
        </div>
        <div class="panel-body">
          <div class="row">
            <form>
              <div class="col-sm-10 col-md-6">
                <div class="form-group">
                  <label for="searchByAcademicYear" class="control-label">Academic Year</label>
                  <select id="searchByAcademicYear" class="studentDropdown form-control"></select>
                </div>
              </div>
              <div class="col-sm-10 col-md-6">
                <div class="form-group">
                  <label for="searchByBatch" class="control-label">Batch</label>
                  <select id="searchByBatch" class="studentDropdown form-control"></select>
                </div>
              </div>
              <div class="col-sm-10 col-md-6">
                <div class="form-group">
                  <label for="searchByExamination" class="control-label">Examination</label>
                  <select id="searchByExamination" class="studentDropdown form-control"></select>
                </div>
              </div>
              <div class="col-sm-12">
                <a href="#" class="btn btn-primary btn-lg" onclick="">Search</a>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="panel panel-primary">
        <div class="panel-heading">
          Gradings
        </div>
        <div class="panel-body">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table foo table-bordered">
                <thead>
                  <tr>
                    <th data-toggle="true">Admin No</th>
                    <th >Name</th>
                    <th data-hide="phone" data-sort-ignore="true">Ds</th>
                  </tr>
                </thead>
                <tr>
                  <td>A</td>
                  <td>80</td>
                  <td></td>
                </tr>
                <tr>
                  <td>B</td>
                  <td>70</td>
                  <td></td>
                </tr>
                <tr>
                  <td>C</td>
                  <td>60</td>
                  <td></td>
                </tr>
                <tr>
                  <td>D</td>
                  <td>50</td>
                  <td></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
