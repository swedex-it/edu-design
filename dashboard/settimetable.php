<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Set Timetable
        <small>Timetable for the batch</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Timetable</li>
        <li class="active">Set Timetable</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="pull-left">
              <button class="btn btn-success">Generate PDF</button>
          </div>
          <div class="text-right">
            <a class="btn btn-primary" href="setweekdays.php">Set Weekdays</a>
            <a class="btn btn-primary tmar-xs" href="setclasstime.php">Set Class Timing</a>
          </div>
        </div>
        <div class="panel-body">
          <div class="box box-primary">
            <div class="box-header">
              <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-2">
                  <strong>Grade <label class="label label-primary">Class 10</label></strong>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 hidden-xs">
                  <strong>Batch <label class="label label-primary">A</label></strong>
                </div>
                <!-- only will be visible on xs screen size -->
                <div class="col-xs-12 visible-xs-block tpad">
                  <strong>Batch <label class="label label-primary">A</label></strong>
                </div>
              </div>
              <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <button class="btn btn-default" data-toggle="modal" data-target="#modalExamByGradeBatch">Change Batch</button>
              </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table foo table-bordered">
                  <thead>
                    <tr>
                      <th data-toggle="true" data-sort-ignore="true">Weekdays</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">08:30 AM - 10: 00 AM</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">10:00 AM - 12: 00 PM</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">01:00 PM - 02: 00 PM</th>
                      <th data-hide="phone,tablet" data-sort-ignore="true">02:00 PM - 03: 00 PM</th>
                    </tr>
                  </thead>
                  <tr>
                    <td>Monday</td>
                    <td>Maths <a href="#" onclick="confirm('Are you sure ?');"><i class="ion-ios-close"></i></a></td>
                    <td>English <a href="#" onclick="confirm('Are you sure ?');"> <i class="ion-ios-close"></i></a></td>
                    <td>Science <a href="#" onclick="confirm('Are you sure ?');"> <i class="ion-ios-close"></i></a></td>
                    <td>Economics <a href="#" onclick="confirm('Are you sure ?');"> <i class="ion-ios-close"></i></a></td>
                  </tr>
                  <tr>
                    <td>Tuesday</td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                  </tr>
                  <tr>
                    <td>Wednesday</td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                  </tr>
                  <tr>
                    <td>Thursday</td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                  </tr>
                  <tr>
                    <td>Friday</td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                    <td><a href="#">Assign</a></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
