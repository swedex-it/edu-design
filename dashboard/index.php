<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Overview</small>
      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li> -->
        <li class="active"><i class="fa fa-dashboard"></i> Home</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-4">
          <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-red"><i class="fa fa-child"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Students</span>
              <span class="info-box-number">600</span>
              <span class="progress-description">
                Total students
              </span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div>
        <div class="col-md-4">
          <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Staffs</span>
              <span class="info-box-number">120</span>
              <span class="progress-description">
                Total staffs
              </span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div>
        <div class="col-md-4">
          <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-orange"><i class="fa fa-users"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Parents</span>
              <span class="info-box-number">550</span>
              <span class="progress-description">
                Total parents
              </span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div>
      </div><!-- /.row -->

      <div class="row">
        <div class="col-md-3">
          <div class="list-group">
            <a href="#" class="list-group-item active">
              Quick links <i class="fa fa-angle-double-right"></i>
            </a>
            <a href="#" class="list-group-item">Mailbox</a>
            <a href="#" class="list-group-item">News</a>
            <a href="#" class="list-group-item">Activity Feed</a>
            <a href="#" class="list-group-item">Event Type</a>
          </div>
        </div>
        <div class="col-md-9">
          <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-calendar"></i> Event Schedule</div>
            <div class="panel-body">
              <div id="calendar"></div>
            </div>
          </div>

        </div>
      </div><!-- /.calendar .row -->

    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
