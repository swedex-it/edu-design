<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student Documents Settings
        <small>Manage Student Document Types</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">Student Documents Settings</li>
      </ol>
    </section>

    <!-- Model form here -->

    <div class="modal fade" id="newStudentDocType">
      <div class="modal-dialog">
        <div class="modal-content">
          <form>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">New Document Type</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="newDocType">Document Type</label>
                <input type="text" class="form-control" id="newDocType" placeholder="Document Type">
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Is Required
                </label>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- End Modal form here -->

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#newStudentDocType">Create Document Types</button>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-repsonsive">
            <table class="table foo table-boredered">
              <thead>
              <tr>
                <th data-toggle="true">Document Name</th>
                <th>Is Required</th>
                <th data-hide="phone" data-sort-ignore="true">Action</th>
              </tr>
              </thead>
              <tr>
                <td>Birth Certificate</td>
                <td>Yes</td>
                <td>
                  <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
              </tr>
              <tr>
                <td>Address Proof</td>
                <td>Yes</td>
                <td>
                  <button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                  <button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
