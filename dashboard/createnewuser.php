<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create User
        <small>Add new user details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Settings</li>
        <li class="active">Create User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="panel panel-primary">
        <div class="panel-heading">
          User Details
        </div>
        <div class="panel-body">
          <form class="form-horizontal">
            <div class="form-group required">
              <label for="username" class="col-sm-2 control-label">Username</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="username" placeholder="Email">
              </div>
            </div>
            <div class="form-group required">
              <label for="password" class="col-sm-2 control-label">Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" id="password" placeholder="Password">
              </div>
            </div>
            <div class="form-group required">
              <label for="email" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-9">
                <input type="email" class="form-control" id="email" placeholder="Email">
              </div>
            </div>
            <div class="form-group required">
              <label for="superuser" class="col-sm-2 control-label">Super user</label>
              <div class="col-sm-9">
                <select  class="form-control" id="superuser" >
                  <option>No</option>
                  <option>Yes</option>
                </select>
                <small>Select 'Yes' only for Admin rights</small>
              </div>
            </div>
            <div class="form-group required">
              <label for="status" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-9">
                <select  class="form-control" id="status" >
                  <option>Active</option>
                  <option>Inactive</option>
                </select>
              </div>
            </div>
            <div class="form-group required">
              <label for="firstName" class="col-sm-2 control-label">First Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="firstName" placeholder="First Name">
              </div>
            </div>
            <div class="form-group required">
              <label for="lastName" class="col-sm-2 control-label">Last Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="lastName" placeholder="Last Name">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-1 col-sm-10 text-center">
                <a type="submit" class="btn btn-primary btn-lg" href="createuserrole.php">Create</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
