<?php include "includes/_header.php"; ?>
<div class="wrapper">

  <?php include "includes/_nav.php"; ?>

  <aside class="main-sidebar">
    <?php include "includes/_sidebar.php"; ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student
        <small>Add new student</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Student</li>
        <li class="active">New student</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row hidden-sm hidden-xs">
        <div class="col-md-offset-2 col-md-8">
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">New Student</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Parent</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-primary btn-nav">Documents</button>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-default btn-nav" disabled="disabled">Preview</button>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading"><strong>Upload Student Documents</strong></div>
            <div class="panel-body">

              <!-- Standar Form -->
              <h4>Select files from your computer</h4>
              <form action="" method="post" enctype="multipart/form-data" id="js-upload-form">
                <div class="form-inline">
                  <div class="form-group">
                    <input type="file" name="files[]" id="js-upload-files" multiple>
                  </div>
                  <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit">Upload files</button>
                </div>
              </form>

              <!-- Drop Zone -->
              <h4>Or drag and drop files below</h4>
              <div class="upload-drop-zone" id="drop-zone">
                Just drag and drop files here
              </div>

              <!-- Progress Bar -->
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                  <span class="sr-only">60% Complete</span>
                </div>
              </div>

              <!-- Upload Finished -->
              <div class="js-upload-finished">
                <h3>Processed files</h3>
                <div class="list-group">
                  <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">Success</span>image-01.jpg</a>
                  <a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">Success</span>image-02.jpg</a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2 col-md-offset-5">
                  <!-- <input type="submit" class="btn-primary" value="DOCUMENTS  >" > -->
                  <a href="studentpreview.php" class="btn btn-primary btn-lg">Preview <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div> <!-- /container -->
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

  <?php include "includes/_footer.php"; ?>

  <?php include "includes/_rightsidebar.php"; ?>
</div><!-- ./wrapper -->
<?php include "includes/_scripttags.php"; ?>
